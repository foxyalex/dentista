$(document).ready(iniciar);

function iniciar() {

	$(".btn-crear").on("click", mostrarModalCrear);
	$("#form-crear").on("submit", crearRegistro);
	$("#form-editar").on("submit", editarRegistro);
	$("#form-eliminar").on("submit", eliminarRegistro);
	$("#tabla-registros").delegate(".btn-editar", "click", verModalEditar);
	$("#tabla-registros").delegate(".btn-eliminar", "click", verModalEliminar);
	$("#tabla-registros").delegate(".btn-departamentos", "click", verDepartamentos);

	$(".btn-crear-departamento").on("click", mostrarModalCrearDepartamento);
	$("#form-crear-departamento").on("submit", crearDepartamento);
	$("#form-editar-departamento").on("submit", editarDepartamento);
	$("#form-eliminar-departamento").on("submit", eliminarDepartamento);
	$("#tabla-departamentos").delegate(".btn-editar-departamento", "click", verModalEditarDepartamento);
	$("#tabla-departamentos").delegate(".btn-eliminar-departamento", "click", verModalEliminarDepartamento);
	$("#tabla-departamentos").delegate(".btn-municipios", "click", verMunicipios);

	$(".btn-crear-municipio").on("click", mostrarModalCrearMunicipio);
	$("#form-crear-municipio").on("submit", crearMunicipio);
	$("#form-editar-municipio").on("submit", editarMunicipio);
	$("#form-eliminar-municipio").on("submit", eliminarMunicipio);
	$("#tabla-municipios").delegate(".btn-editar-municipio", "click", verModalEditarMunicipio);
	$("#tabla-municipios").delegate(".btn-eliminar-municipio", "click", verModalEliminarMunicipio);

	$(".close-departamentos").on("click", closeDepartamentos);
	$(".close-municipios").on("click", closeMunicipios);

	llenarTabla();

    $('#tabla-registros, #tabla-departamentos, #tabla-municipios').DataTable({
		responsive: true,
		"oLanguage": {
			"sLengthMenu": "Mostrando _MENU_ filas",
		  	"sSearch": "",
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
		  			"sFirst":    "Primero",
		  			"sLast":     "Último",
		  			"sNext":     "Siguiente",
		  			"sPrevious": "Anterior"
				}
		}
    });
}

function llenarTabla( e ) 
{
	$.ajax({
		type:'GET',
		url:'api/paises',
		dataType:'json',
		data:{},
		success:function( registros )
		{
			
			$("#tabla-registros").dataTable().fnClearTable();

			var xIndex = 0;
			$.each( registros, function(index, value){
				var acciones;


			
				acciones = "<div class='btn-tollbar' role='toolbar'>" +
							"<div class='btn-group' role='group'>" +
								"<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-editar'><i class='fa fa-pencil-square-o'></i> Editar</a> " +
								"<a href='#' idreg='"+value.id+"' nombre='"+value.nombre+"' class='btn btn-default btn-sm btn-departamentos'><i class='fa fa-crosshairs'></i> Departamentos</a> " +
								"<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-eliminar'><i class='fa fa-trash'></i> Eliminar</a> " +
							"</div>" +
						"</div>";
			
				
				$("#tabla-registros").dataTable().fnAddData([
					++xIndex,
					value.nombre,
					acciones
				]);
			});
		},
		error: function( error )
		{
			console.log(error);
		}
	});
	

	if ( e != null )
		e.preventDefault();
}

function mostrarModalCrear( e ) {

	$("#modal-crear").modal('show');
	$("#form-crear")[0].reset();
	if(e!=null)
		e.preventDefault();
}

function crearRegistro( e )
{
	$.ajax({
		type: 		"POST",
		url: 		"api/paises",
		dataType: 	"json",
		data: 		$(this).serialize(),
		success: function( respuesta )
		{
			$("#modal-crear").modal("hide");
			setTimeout( function(){ ratPack.refresh(); }, 300 );
		},
		error: function( error )
		{
			console.log(error)
		}
	});

	if ( e != null )
		e.preventDefault();
		e.stopPropagation();
}

function verModalEditar( e )
{
	var idreg = $(e.target).closest("a").attr("idreg");
	$.ajax({
		type: 		"GET",
		url: 		"api/paises/"+idreg,
		dataType: 	"json",
		data: 		{},
		success: function ( registro )
		{
			$("#modal-editar").modal("show");
			$("#modal-editar #form-editar #nombre").val(registro.nombre);
			$("#modal-editar #form-editar #descripcion").val(registro.descripcion);
			$("#modal-editar #form-editar #updateid").val(registro.id);
		},
		error: function ( error )
		{
			console.log(error);
		}
	});

	if ( e != null)
		e.preventDefault();
}

function editarRegistro( e )
{
	var idreg = $("#modal-editar #form-editar #updateid").val();
	$.ajax({
		type: 		"PUT",
		url: 		"api/paises/"+idreg,
		dataType: 	"json",
		data: 		$(this).serialize(),
		success: function( registro )
		{
			$("#modal-editar").modal("hide");
			setTimeout( function(){ ratPack.refresh(); }, 300 );
			
		},
		error: function( error )
		{
			console.log(error)
		}
	});

	if ( e != null )
		e.preventDefault();
		e.stopPropagation();
}

function verModalEliminar( e ) {

	var idreg = $(e.target).closest("a").attr("idreg");
	$("#modal-eliminar").modal('show');
	$("#modal-eliminar #form-eliminar #updateid").val(idreg);

	if ( e != null)
		e.preventDefault();
}

function eliminarRegistro( e ) {
	var idreg = $("#modal-eliminar #form-eliminar #updateid").val();
	$.ajax({
		type: 		"DELETE",
		url: 		"api/paises/"+idreg,
		dataType: 	"json",
		data: 		{},
		success: function( respuesta )
		{
			$("#modal-eliminar").modal("hide");
			setTimeout( function(){ ratPack.refresh(); }, 300 );
		},
		error: function( error )
		{
			console.log(error)
		}
	});

	if ( e != null )
		e.preventDefault();
		e.stopPropagation();
}

function verDepartamentos(e) {
	var idreg = $(e.target).closest("a").attr("idreg");
	var nombre = $(e.target).closest("a").attr("nombre");
	$.ajax({
		type: 		"GET",
		url: 		"api/paises/"+idreg+"/departamentos",
		dataType: 	"json",
		data: 		{},
		success: function ( departamentos )
		{
			$("#block-departamentos").removeClass("hide");
			$("#block-municipios").addClass("hide");
			$("#departamento_actual").val(idreg);
			$("#form-crear-departamento #paisId").val(idreg);
			$("#form-editar-departamento #paisId").val(idreg);
			$("#title-departamento").text("Departamentos " + nombre);
			llenarTablaDepartamentos(departamentos);
		},
		error: function ( error )
		{
			console.log(error);
		}
	});

	if ( e != null)
		e.preventDefault();
}

 function llenarTablaDepartamentos( departamentos ) {
	$("#tabla-departamentos").dataTable().fnClearTable();
	var xIndex = 0;
	$.each( departamentos, function(index, value){
		var acciones;	
		acciones = "<div class='btn-tollbar' role='toolbar'>" +
					"<div class='btn-group' role='group'>" +
						"<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-editar-departamento'><i class='fa fa-pencil-square-o'></i> Editar</a> " +
						"<a href='#' idreg='"+value.id+"' nombre='"+value.nombre+"' class='btn btn-default btn-sm btn-municipios'><i class='fa fa-crosshairs'></i> Municipios</a> " +
						"<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-eliminar-departamento'><i class='fa fa-trash'></i> Eliminar</a> " +
					"</div>" +
				"</div>";
	
		$("#tabla-departamentos").dataTable().fnAddData([
			++xIndex,
			value.nombre,
			acciones
		]);
	});
}

function descargarDepartamentos() {
	var idreg = $("#departamento_actual").val();
	$.ajax({
		type: 		"GET",
		url: 		"api/paises/"+idreg+"/departamentos",
		dataType: 	"json",
		data: 		{},
		success: function ( departamentos )
		{
			llenarTablaDepartamentos(departamentos);
		},
		error: function ( error )
		{
			console.log(error);
		}
	});

	if ( e != null)
		e.preventDefault();
}

function mostrarModalCrearDepartamento( e ) {
	$("#modal-crear-departamento").modal('show');
	$("#form-crear-departamento")[0].reset();
	if(e!=null)
		e.preventDefault();
}

function crearDepartamento( e ) {
	$.ajax({
		type: 		"POST",
		url: 		"api/departamentos",
		dataType: 	"json",
		data: 		$(this).serialize(),
		success: function( respuesta )
		{
			$("#modal-crear-departamento").modal("hide");
			descargarDepartamentos();
		},
		error: function( error )
		{
			console.log(error)
		}
	});

	if ( e != null )
		e.preventDefault();
		e.stopPropagation();
}

function verModalEditarDepartamento( e ) {
	var idreg = $(e.target).closest("a").attr("idreg");
	$.ajax({
		type: 		"GET",
		url: 		"api/departamentos/"+idreg,
		dataType: 	"json",
		data: 		{},
		success: function ( registro )
		{
			$("#modal-editar-departamento").modal("show");
			$("#modal-editar-departamento #form-editar-departamento #nombre").val(registro.nombre);
			$("#modal-editar-departamento #form-editar-departamento #updateid").val(registro.id);
		},
		error: function ( error )
		{
			console.log(error);
		}
	});

	if ( e != null)
		e.preventDefault();
}

function editarDepartamento( e ) {
	var idreg = $("#modal-editar-departamento #form-editar-departamento #updateid").val();
	$.ajax({
		type: 		"PUT",
		url: 		"api/departamentos/"+idreg,
		dataType: 	"json",
		data: 		$(this).serialize(),
		success: function( registro )
		{
			$("#modal-editar-departamento").modal("hide");
			descargarDepartamentos();
			
		},
		error: function( error )
		{
			console.log(error)
		}
	});

	if ( e != null )
		e.preventDefault();
		e.stopPropagation();
}

function verModalEliminarDepartamento( e ) {

	var idreg = $(e.target).closest("a").attr("idreg");
	$("#modal-eliminar-departamento").modal('show');
	$("#modal-eliminar-departamento #form-eliminar-departamento #updateid").val(idreg);

	if ( e != null)
		e.preventDefault();
}

function eliminarDepartamento( e ) {
	var idreg = $("#modal-eliminar-departamento #form-eliminar-departamento #updateid").val();
	$.ajax({
		type: 		"DELETE",
		url: 		"api/departamentos/"+idreg,
		dataType: 	"json",
		data: 		{},
		success: function( respuesta )
		{
			$("#modal-eliminar-departamento").modal("hide");
			descargarDepartamentos();
		},
		error: function( error )
		{
			console.log(error)
		}
	});

	if ( e != null )
		e.preventDefault();
		e.stopPropagation();
}

function verMunicipios(e) {
	var idreg = $(e.target).closest("a").attr("idreg");
	var nombre = $(e.target).closest("a").attr("nombre");
	$.ajax({
		type: 		"GET",
		url: 		"api/departamentos/"+idreg+"/municipios",
		dataType: 	"json",
		data: 		{},
		success: function ( municipios )
		{
			$("#block-municipios").removeClass("hide");
			$("#municipio_actual").val(idreg);
			$("#form-crear-municipio #departamentoId").val(idreg);
			$("#form-editar-municipio #departamentoId").val(idreg);
			$("#title-municipio").text("Municipios " + nombre);
			llenarTablaMunicipios(municipios);
		},
		error: function ( error )
		{
			console.log(error);
		}
	});

	if ( e != null)
		e.preventDefault();
}

 function llenarTablaMunicipios( municipios ) {
	$("#tabla-municipios").dataTable().fnClearTable();
	var xIndex = 0;
	$.each( municipios, function(index, value){
		var acciones;	
		acciones = "<div class='btn-tollbar' role='toolbar'>" +
					"<div class='btn-group' role='group'>" +
						"<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-editar-municipio'><i class='fa fa-pencil-square-o'></i> Editar</a> " +
						"<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-eliminar-municipio'><i class='fa fa-trash'></i> Eliminar</a> " +
					"</div>" +
				"</div>";
	
		$("#tabla-municipios").dataTable().fnAddData([
			++xIndex,
			value.nombre,
			acciones
		]);
	});
}

function descargarMunicipios() {
	var idreg = $("#municipio_actual").val();
	$.ajax({
		type: 		"GET",
		url: 		"api/departamentos/"+idreg+"/municipios",
		dataType: 	"json",
		data: 		{},
		success: function ( municipios )
		{
			llenarTablaMunicipios(municipios);
		},
		error: function ( error )
		{
			console.log(error);
		}
	});

	if ( e != null)
		e.preventDefault();
}

function mostrarModalCrearMunicipio( e ) {
	$("#modal-crear-municipio").modal('show');
	$("#form-crear-municipio")[0].reset();
	if(e!=null)
		e.preventDefault();
}

function crearMunicipio( e ) {
	$.ajax({
		type: 		"POST",
		url: 		"api/municipios",
		dataType: 	"json",
		data: 		$(this).serialize(),
		success: function( respuesta )
		{
			$("#modal-crear-municipio").modal("hide");
			descargarMunicipios();
		},
		error: function( error )
		{
			console.log(error)
		}
	});

	if ( e != null )
		e.preventDefault();
		e.stopPropagation();
}

function verModalEditarMunicipio( e ) {
	var idreg = $(e.target).closest("a").attr("idreg");
	$.ajax({
		type: 		"GET",
		url: 		"api/municipios/"+idreg,
		dataType: 	"json",
		data: 		{},
		success: function ( registro )
		{
			$("#modal-editar-municipio").modal("show");
			$("#modal-editar-municipio #form-editar-municipio #nombre").val(registro.nombre);
			$("#modal-editar-municipio #form-editar-municipio #updateid").val(registro.id);
		},
		error: function ( error )
		{
			console.log(error);
		}
	});

	if ( e != null)
		e.preventDefault();
}

function editarMunicipio( e ) {
	var idreg = $("#modal-editar-municipio #form-editar-municipio #updateid").val();
	$.ajax({
		type: 		"PUT",
		url: 		"api/municipios/"+idreg,
		dataType: 	"json",
		data: 		$(this).serialize(),
		success: function( registro )
		{
			$("#modal-editar-municipio").modal("hide");
			descargarMunicipios();
		},
		error: function( error )
		{
			console.log(error)
		}
	});

	if ( e != null )
		e.preventDefault();
		e.stopPropagation();
}

function verModalEliminarMunicipio( e ) {

	var idreg = $(e.target).closest("a").attr("idreg");
	$("#modal-eliminar-municipio").modal('show');
	$("#modal-eliminar-municipio #form-eliminar-municipio #updateid").val(idreg);

	if ( e != null)
		e.preventDefault();
}

function eliminarMunicipio( e ) {
	var idreg = $("#modal-eliminar-municipio #form-eliminar-municipio #updateid").val();
	$.ajax({
		type: 		"DELETE",
		url: 		"api/municipios/"+idreg,
		dataType: 	"json",
		data: 		{},
		success: function( respuesta )
		{
			$("#modal-eliminar-municipio").modal("hide");
			descargarMunicipios();
		},
		error: function( error )
		{
			console.log(error)
		}
	});

	if ( e != null )
		e.preventDefault();
		e.stopPropagation();
}

function closeDepartamentos( e ) {
	$("#block-departamentos").addClass("hide");
	$("#block-municipios").addClass("hide");

	if ( e != null )
		e.preventDefault();
}

function closeMunicipios( e ) {
	$("#block-municipios").addClass("hide");
	if ( e != null )
		e.preventDefault();
}
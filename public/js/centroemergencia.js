$(document).ready(iniciar);

var esNuevo = true;
var telefonoNuevo = true;
var correoNuevo = true;
var direccionNueva = true;
var facturacionNueva = true;

function iniciar() {

    $(".btn-crear").on("click", verBloqueCentroEmergencia);
    $(".close-centroemergencia").on("click", closeBlockCentroEmergencia);
    $("#form-datosgenerales").on("submit", guardarCentroEmergencia);
	$("#form-telefonos").on("submit", crearTelefono);
	$("#form-correos").on("submit", crearCorreo);
	$("#tabla-registros").delegate(".btn-editar", "click", verBloquePacienteEditar);
	$("#tabla-telefonos").delegate(".btn-editar-telefono", "click", editarTelefono);
	$("#tabla-telefonos").delegate(".btn-eliminar-telefono", "click", eliminarTelefono);
	$("#tabla-correos").delegate(".btn-editar-correo", "click", editarCorreo);
	$("#tabla-correos").delegate(".btn-eliminar-correo", "click", eliminarCorreo);

	$(".btn-ctel").on("click", cancelarEditarTelefono);
	$(".btn-ccor").on("click", cancelarEditarCorreo);

    llenarTabla();
    llenarTipoTelefono();
    llenarTipoCorreo();

    $('.mydatepicker').datepicker();
    
    $('#tabla-registros, #tabla-telefonos, #tabla-correos').DataTable({
		responsive: true,
		"oLanguage": {
			"sLengthMenu": "Mostrando _MENU_ filas",
		  	"sSearch": "",
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
		  			"sFirst":    "Primero",
		  			"sLast":     "Último",
		  			"sNext":     "Siguiente",
		  			"sPrevious": "Anterior"
				}
		}
    });
}

function verBloqueCentroEmergencia( e ) {

    esNuevo = true;
    $("#block-centroemergencia").removeClass('hide');
	$("#form-datosgenerales")[0].reset();
	$("#btn-fdg").text("Crear Centro Emergencia");
    bloquearTabs();

    if ( e != null )
		e.preventDefault();
}

function guardarCentroEmergencia( e ) {

    $.ajax({
		type: 		(esNuevo) ? "POST" : "PUT",
		url: 		(esNuevo) ? "api/centroemergencia" : "api/centroemergencia/"+ $("#centroemergenciaid").val(),
		dataType: 	"json",
		data: 		$(this).serialize(),
		success: function( respuesta )
		{
			if ( esNuevo ) {
				$("#centroemergenciaid").val(respuesta.id);
            	llenarTabla();
            	habilitarTabs();
			} else {
				llenarTabla();	
			}
		},
		error: function( error )
		{
			console.log(error)
		}
	});

    if ( e != null )
		e.preventDefault();
        e.stopPropagation();
}

function verBloquePacienteEditar( e ) {
	var idreg = $(e.target).closest("a").attr("idreg");
	$.ajax({
		type: 		"GET",
		url: 		"api/centroemergencia/"+idreg,
		dataType: 	"json",
		data: 		{},
		success: function ( centroemergencia )
		{
			esNuevo = false;
			$("#block-centroemergencia").removeClass('hide');
			$("#btn-fdg").text("Actualizar Centro Emergencia");
			$("#centroemergenciaid").val(centroemergencia.id);
			habilitarTabs();
			llenarInformaciónCentroEmergencia(centroemergencia);
		},
		error: function ( error )
		{
			console.log(error);
		}
	});

	if ( e != null)
		e.preventDefault();
}

function llenarInformaciónCentroEmergencia( centroemergencia ) {
	console.log(centroemergencia);
	$("#form-datosgenerales #nombre").val(centroemergencia.nombre);
	$("#form-datosgenerales #estado").val(centroemergencia.estado);

	llenarTelefonos(centroemergencia.telefonos);
	llenarCorreos(centroemergencia.correos);
}

/** TELEFONOS */

function llenarTelefonos( telefonos ) {
	$("#tabla-telefonos").dataTable().fnClearTable();

	var xIndex = 0;
	$.each( telefonos, function(index, value){
		var acciones;
		acciones = "<div class='btn-tollbar' role='toolbar'>" +
					"<div class='btn-group' role='group'>" +
						"<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-editar-telefono'><i class='fa fa-pencil-square-o'></i> Editar</a> " +
						"<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-eliminar-telefono'><i class='fa fa-trash-o'></i> Eliminar</a> " +
					"</div>" +
				"</div>";

		var itemPreferida = "<span class='label label-warning'>No preferida</span>";

		if (value.preferida) {
			itemPreferida = "<span class='label label-info'>Preferida</span>";
		}
		
		$("#tabla-telefonos").dataTable().fnAddData([
			++xIndex,
			value.numero,
			value.tipotelefono.nombre,
			itemPreferida,
			acciones
		]);
	});
}

function descargarTelefonos( centroemergencia ) {
	$.ajax({
		type: 		"GET",
		url: 		"api/centroemergencia/"+centroemergencia+"/telefonos",
		dataType: 	"json",
		data: 		{},
		success: function ( telefonos )
		{
			llenarTelefonos(telefonos);
		},
		error: function ( error )
		{
			console.log(error);
		}
	});
}

function crearTelefono( e ) {
	$.ajax({
		type: 		(telefonoNuevo) ? "POST" : "PUT",
		url: 		(telefonoNuevo) ? "api/centroemergenciatelefono" : "api/centroemergenciatelefono/"+ $("#form-telefonos #updateid").val(),
		dataType: 	"json",
		data: 		$(this).serialize()+"&centroemergencia="+$("#centroemergenciaid").val(),
		success: function( respuesta )
		{
			descargarTelefonos($("#centroemergenciaid").val());
			$("#form-telefonos")[0].reset();
			$("#form-telefonos .btn-ftel").text("Agregar Telefono");
			$("#form-telefonos .btn-ctel").addClass("hide");
			telefonoNuevo = true;
		},
		error: function( error )
		{
			console.log(error)
		}
	});

    if ( e != null )
		e.preventDefault();
        e.stopPropagation();
}

function editarTelefono( e ) {
	var idreg = $(e.target).closest("a").attr("idreg");
	$.ajax({
		type: 		"GET",
		url: 		"api/centroemergenciatelefono/"+idreg,
		dataType: 	"json",
		data: 		{},
		success: function ( respuesta )
		{
			$("#form-telefonos #updateid").val(respuesta.id);
			$("#form-telefonos #numero").val(respuesta.numero);
			$("#form-telefonos #tipotelefono").val(respuesta.tipotelefono);
			$("#form-telefonos #preferida").val(respuesta.preferida);
			$("#form-telefonos .btn-ftel").text("Editar Telefono");
			$("#form-telefonos .btn-ctel").removeClass("hide");
			telefonoNuevo = false;
		},
		error: function ( error )
		{
			console.log(error);
		}
	});

	if ( e != null )
		e.preventDefault();
}

function cancelarEditarTelefono( e ) {
	$("#form-telefonos")[0].reset();
	$("#form-telefonos .btn-ftel").text("Agregar Telefono");
	$("#form-telefonos .btn-ctel").addClass("hide");
	telefonoNuevo = true;
}

function eliminarTelefono( e ) {
	var idreg = $(e.target).closest("a").attr("idreg");
	$.ajax({
		type: 		"DELETE",
		url: 		"api/centroemergenciatelefono/"+idreg,
		dataType: 	"json",
		data: 		{},
		success: function ( respuesta )
		{
			descargarTelefonos($("#centroemergenciaid").val());
		},
		error: function ( error )
		{
			console.log(error);
		}
	});

	if ( e != null )
		e.preventDefault();
}

/** CORREOS */

function llenarCorreos( correos ) {
	$("#tabla-correos").dataTable().fnClearTable();

	var xIndex = 0;
	$.each( correos, function(index, value){
		var acciones;
		acciones = "<div class='btn-tollbar' role='toolbar'>" +
					"<div class='btn-group' role='group'>" +
						"<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-editar-correo'><i class='fa fa-pencil-square-o'></i> Editar</a> " +
						"<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-eliminar-correo'><i class='fa fa-trash-o'></i> Eliminar</a> " +
					"</div>" +
				"</div>";

		var itemPreferida = "<span class='label label-warning'>No preferida</span>";

		if (value.preferida) {
			itemPreferida = "<span class='label label-info'>Preferida</span>";
		}
		
		$("#tabla-correos").dataTable().fnAddData([
			++xIndex,
			value.correo,
			value.tipocorreo.nombre,
			itemPreferida,
			acciones
		]);
	});
}

function descargarCorreos( centroemergencia ) {
	$.ajax({
		type: 		"GET",
		url: 		"api/centroemergencia/"+centroemergencia+"/correos",
		dataType: 	"json",
		data: 		{},
		success: function ( correos )
		{
			llenarCorreos(correos);
		},
		error: function ( error )
		{
			console.log(error);
		}
	});
}

function crearCorreo( e ) {
	$.ajax({
		type: 		(correoNuevo) ? "POST" : "PUT",
		url: 		(correoNuevo) ? "api/centroemergenciacorreo" : "api/centroemergenciacorreo/"+ $("#form-correos #updateid").val(),
		dataType: 	"json",
		data: 		$(this).serialize()+"&centroemergencia="+$("#centroemergenciaid").val(),
		success: function( respuesta )
		{
			descargarCorreos($("#centroemergenciaid").val());
			$("#form-correos")[0].reset();
			$("#form-correos #btn-fcor").text("Agregar Correo");
			$("#form-correos .btn-ccor").addClass("hide");
			correoNuevo = true;
		},
		error: function( error )
		{
			console.log(error)
		}
	});

    if ( e != null )
		e.preventDefault();
        e.stopPropagation();
}

function editarCorreo( e ) {
	var idreg = $(e.target).closest("a").attr("idreg");
	$.ajax({
		type: 		"GET",
		url: 		"api/centroemergenciacorreo/"+idreg,
		dataType: 	"json",
		data: 		{},
		success: function ( respuesta )
		{
			$("#form-correos #updateid").val(respuesta.id);
			$("#form-correos #correo").val(respuesta.correo);
			$("#form-correos #tipocorreo").val(respuesta.tipocorreo);
			$("#form-correos #preferida").val(respuesta.preferida);
			$("#form-correos .btn-fcor").text("Editar Correo");
			$("#form-correos .btn-ccor").removeClass("hide");
			correoNuevo = false;
		},
		error: function ( error )
		{
			console.log(error);
		}
	});

	if ( e != null )
		e.preventDefault();
}

function cancelarEditarCorreo( e ) {
	$("#form-correos")[0].reset();
	$("#form-correos #btn-fcor").text("Agregar Correo");
	$("#form-correos .btn-ccor").addClass("hide");
	telefonoNuevo = true;
}

function eliminarCorreo( e ) {
	var idreg = $(e.target).closest("a").attr("idreg");
	$.ajax({
		type: 		"DELETE",
		url: 		"api/centroemergenciacorreo/"+idreg,
		dataType: 	"json",
		data: 		{},
		success: function ( respuesta )
		{
			descargarCorreos($("#personaid").val());
		},
		error: function ( error )
		{
			console.log(error);
		}
	});

	if ( e != null )
		e.preventDefault();
}


function bloquearTabs() {
    $(".litel").addClass("disabled");
    $(".licor").addClass("disabled");

    $(".tabtel").removeAttr("data-toggle");
    $(".tabcor").removeAttr("data-toggle");

    $(".tabtel").removeAttr("href");
    $(".tabcor").removeAttr("href");
}

function habilitarTabs() {
    $(".litel").removeClass("disabled");
    $(".licor").removeClass("disabled");

    $(".tabtel").attr("data-toggle", "tab");
    $(".tabcor").attr("data-toggle", "tab");

    $(".tabtel").attr("href", "#telefonos");
    $(".tabcor").attr("href", "#correos");
}

function llenarTabla( e ) 
{
	$.ajax({
		type:'GET',
		url:'api/centroemergencia',
		dataType:'json',
		data:{},
		success:function( registros )
		{
			
			$("#tabla-registros").dataTable().fnClearTable();

			var xIndex = 0;
			$.each( registros, function(index, value){
				var acciones;
                acciones = "<div class='btn-tollbar' role='toolbar'>" +
                            "<div class='btn-group' role='group'>" +
                                "<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-editar'><i class='fa fa-pencil-square-o'></i> Editar</a> " +
                            "</div>" +
                        "</div>";

				var estado = "<span class='label label-danger'>No Activo</span>";

				if (value.estado) {
					estado = "<span class='label label-info'>Activo</span>";
				}
				
				$("#tabla-registros").dataTable().fnAddData([
					++xIndex,
					value.nombre,
					estado,
					acciones
				]);
			});
			
		},
		error: function( error )
		{
			console.log(error);
		}
	});
	

	if ( e != null )
		e.preventDefault();
}

function llenarTipoTelefono( e ) {
    $.ajax({
		type:'GET',
		url:'api/tipotelefono',
		dataType:'json',
		data:{},
		success:function( registros )
		{
			var optionTipoProfesion = $("#form-telefonos #tipotelefono");
            optionTipoProfesion.find('option').remove();
            $.each(registros, function(index, value){
                optionTipoProfesion.append($("<option />").val(value.id).text(value.nombre));
            });
		},
		error: function( error )
		{
			console.log(error);
		}
	});
	

	if ( e != null )
		e.preventDefault();
}

function llenarTipoCorreo( e ) {
    $.ajax({
		type:'GET',
		url:'api/tipocorreo',
		dataType:'json',
		data:{},
		success:function( registros )
		{
			var optionTipoProfesion = $("#form-correos #tipocorreo");
            optionTipoProfesion.find('option').remove();
            $.each(registros, function(index, value){
                optionTipoProfesion.append($("<option />").val(value.id).text(value.nombre));
            });
		},
		error: function( error )
		{
			console.log(error);
		}
	});
	

	if ( e != null )
		e.preventDefault();
}

function closeBlockCentroEmergencia( e ) {
	$("#block-centroemergencia").addClass("hide");
	if ( e != null )
		e.preventDefault();
}
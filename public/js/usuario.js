$(document).ready(iniciar);

var esNuevo = true;
var telefonoNuevo = true;
var correoNuevo = true;
var direccionNueva = true;
var facturacionNueva = true;

function iniciar() {

    $(".btn-crear").on("click", verBloqueUsuario);
    $(".close-usuario").on("click", closeBlockUsuario);
    $("#form-datosgenerales").on("submit", guardarUsuario);
	$("#form-telefonos").on("submit", crearTelefono);
	$("#form-correos").on("submit", crearCorreo);
	$("#form-direcciones").on("submit", crearDireccion);
    $("#form-datosaccesos").on("submit", actualizarDatosAcceso);
	$("#tabla-registros").delegate(".btn-editar", "click", verBloqueUsuarioEditar);
	$("#tabla-telefonos").delegate(".btn-editar-telefono", "click", editarTelefono);
	$("#tabla-telefonos").delegate(".btn-eliminar-telefono", "click", eliminarTelefono);
	$("#tabla-correos").delegate(".btn-editar-correo", "click", editarCorreo);
	$("#tabla-correos").delegate(".btn-eliminar-correo", "click", eliminarCorreo);
	$("#tabla-direcciones").delegate(".btn-editar-direccion", "click", editarDireccion);
	$("#tabla-direcciones").delegate(".btn-eliminar-direccion", "click", eliminarDireccion);
	$("#form-direcciones #pais").on("change", llenarDepartamentos);
	$("#form-direcciones #departamento").on("change", llenarMunicipios);

	$(".btn-ctel").on("click", cancelarEditarTelefono);
	$(".btn-cdir").on("click", cancelarEditarDireccion);
	$(".btn-ccor").on("click", cancelarEditarCorreo);

    llenarTabla();
    llenarProfesiones();
    llenarTipoDireccion();
    llenarTipoTelefono();
    llenarTipoCorreo();
	llenarPais();

    $('.mydatepicker').datepicker();
    
    $('#tabla-registros, #tabla-direcciones, #tabla-telefonos, #tabla-correos').DataTable({
		responsive: true,
		"oLanguage": {
			"sLengthMenu": "Mostrando _MENU_ filas",
		  	"sSearch": "",
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
		  			"sFirst":    "Primero",
		  			"sLast":     "Último",
		  			"sNext":     "Siguiente",
		  			"sPrevious": "Anterior"
				}
		}
    });
}

function verBloqueUsuario( e ) {

    esNuevo = true;
    $("#block-usuario").removeClass('hide');
	$("#form-datosgenerales")[0].reset();
	$("#btn-fdg").text("Crear Usuario");
    bloquearTabs();

    if ( e != null )
		e.preventDefault();
}

function guardarUsuario( e ) {

    $.ajax({
		type: 		(esNuevo) ? "POST" : "PUT",
		url: 		(esNuevo) ? "api/persona" : "api/persona/"+ $("#personaid").val(),
		dataType: 	"json",
		data: 		$(this).serialize(),
		success: function( respuesta )
		{
			if ( esNuevo ) {
				$("#personaid").val(respuesta.id);
            	crearUsuario(respuesta);
			} else {
				llenarTabla();
				swal("Buen Trabajo!", "Registro actualizado correctamento!", "success");
			}
		},
		error: function( error )
		{
			console.log(error);
			swal("Opps!", error.responseJSON.message, "error");
		}
	});

    if ( e != null )
		e.preventDefault();
        e.stopPropagation();
}

function crearUsuario( persona ) {
	$.ajax({
		type: 		"POST",
		url: 		"api/usuario",
		dataType: 	"json",
		data: 		{persona: persona.id},
		success: function( respuesta )
		{
            $("#usuarioid").val(respuesta.id);
			llenarTabla();
            habilitarTabs();
			swal("Buen Trabajo!", "Registro creado correctamento!", "success");
		},
		error: function( error )
		{
			swal("Opps!", error.responseJSON.message, "error");
		}
	});
}

function verBloqueUsuarioEditar( e ) {
	var idreg = $(e.target).closest("a").attr("idreg");
	$.ajax({
		type: 		"GET",
		url: 		"api/usuario/"+idreg,
		dataType: 	"json",
		data: 		{},
		success: function ( usuario )
		{
			esNuevo = false;
			$("#block-usuario").removeClass('hide');
			$("#btn-fdg").text("Actualizar Usuario");
			$("#usuarioid").val(usuario.id);
			$("#personaid").val(usuario.persona);
			habilitarTabs();
			llenarInformacionUsuario(usuario);
		},
		error: function ( error )
		{
			console.log(error);
		}
	});

	if ( e != null)
		e.preventDefault();
}

function actualizarDatosAcceso( e ) {
    $.ajax({
		type: 		"PUT",
		url: 		"api/usuario/"+ $("#usuarioid").val(),
		dataType: 	"json",
		data: 		$(this).serialize(),
		success: function( respuesta )
		{
			swal("Buen Trabajo!", "Registro actualizado correctamento!", "success");
		},
		error: function( error )
		{
			console.log(error);
			swal("Opps!", error.responseJSON.message, "error");
		}
	});

    if ( e != null )
		e.preventDefault();
        e.stopPropagation();
}

function llenarInformacionUsuario( usuario ) {
	$("#form-datosgenerales #nombres").val(usuario.persona_completa.nombres);
	$("#form-datosgenerales #apellidos").val(usuario.persona_completa.apellidos);
	$("#form-datosgenerales #casada").val(usuario.persona_completa.casada);
	$("#form-datosgenerales #dni").val(usuario.persona_completa.dni);
	$("#form-datosgenerales #nacimiento").val(usuario.persona_completa.nacimiento);
	$("#form-datosgenerales #profesion").val(usuario.persona_completa.profesion);
	$("#form-datosgenerales #genero").val(usuario.persona_completa.genero);
	$("#form-datosgenerales #estadocivil").val(usuario.persona_completa.estadocivil);
    $("#form-datosaccesos #login").val(usuario.login);

	llenarTelefonos(usuario.persona_completa.telefonos);
	llenarCorreos(usuario.persona_completa.correos);
	llenarDirecciones(usuario.persona_completa.direcciones);
}

/** TELEFONOS */

function llenarTelefonos( telefonos ) {
	$("#tabla-telefonos").dataTable().fnClearTable();

	var xIndex = 0;
	$.each( telefonos, function(index, value){
		var acciones;
		acciones = "<div class='btn-tollbar' role='toolbar'>" +
					"<div class='btn-group' role='group'>" +
						"<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-editar-telefono'><i class='fa fa-pencil-square-o'></i> Editar</a> " +
						"<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-eliminar-telefono'><i class='fa fa-trash-o'></i> Eliminar</a> " +
					"</div>" +
				"</div>";

		var itemPreferida = "<span class='label label-warning'>No preferida</span>";

		if (value.preferida) {
			itemPreferida = "<span class='label label-info'>Preferida</span>";
		}
		
		$("#tabla-telefonos").dataTable().fnAddData([
			++xIndex,
			value.numero,
			value.tipotelefono.nombre,
			itemPreferida,
			acciones
		]);
	});
}

function descargarTelefonos( persona ) {
	$.ajax({
		type: 		"GET",
		url: 		"api/persona/"+persona+"/telefonos",
		dataType: 	"json",
		data: 		{},
		success: function ( telefonos )
		{
			llenarTelefonos(telefonos);
		},
		error: function ( error )
		{
			console.log(error);
		}
	});
}

function crearTelefono( e ) {
	$.ajax({
		type: 		(telefonoNuevo) ? "POST" : "PUT",
		url: 		(telefonoNuevo) ? "api/personatelefono" : "api/personatelefono/"+ $("#form-telefonos #updateid").val(),
		dataType: 	"json",
		data: 		$(this).serialize()+"&persona="+$("#personaid").val(),
		success: function( respuesta )
		{
			descargarTelefonos($("#personaid").val());
			$("#form-telefonos")[0].reset();
			$("#form-telefonos .btn-ftel").text("Agregar Telefono");
			$("#form-telefonos .btn-ctel").addClass("hide");
			telefonoNuevo = true;
			swal("Buen Trabajo!", "Registro ingresado correctamento!", "success");
			swal("Opps!", error.responseJSON.message, "error");
		},
		error: function( error )
		{
			console.log(error);
			swal("Opps!", error.responseJSON.message, "error");
		}
	});

    if ( e != null )
		e.preventDefault();
        e.stopPropagation();
}

function editarTelefono( e ) {
	var idreg = $(e.target).closest("a").attr("idreg");
	$.ajax({
		type: 		"GET",
		url: 		"api/personatelefono/"+idreg,
		dataType: 	"json",
		data: 		{},
		success: function ( respuesta )
		{
			$("#form-telefonos #updateid").val(respuesta.id);
			$("#form-telefonos #numero").val(respuesta.numero);
			$("#form-telefonos #tipotelefono").val(respuesta.tipotelefono);
			$("#form-telefonos #preferida").val(respuesta.preferida);
			$("#form-telefonos .btn-ftel").text("Editar Telefono");
			$("#form-telefonos .btn-ctel").removeClass("hide");
			telefonoNuevo = false;
		},
		error: function ( error )
		{
			console.log(error);
		}
	});

	if ( e != null )
		e.preventDefault();
}

function cancelarEditarTelefono( e ) {
	$("#form-telefonos")[0].reset();
	$("#form-telefonos .btn-ftel").text("Agregar Telefono");
	$("#form-telefonos .btn-ctel").addClass("hide");
	telefonoNuevo = true;
}

function eliminarTelefono( e ) {
	var idreg = $(e.target).closest("a").attr("idreg");
	$.ajax({
		type: 		"DELETE",
		url: 		"api/personatelefono/"+idreg,
		dataType: 	"json",
		data: 		{},
		success: function ( respuesta )
		{
			descargarTelefonos($("#personaid").val());
		},
		error: function ( error )
		{
			console.log(error);
		}
	});

	if ( e != null )
		e.preventDefault();
}

/** CORREOS */

function llenarCorreos( correos ) {
	$("#tabla-correos").dataTable().fnClearTable();

	var xIndex = 0;
	$.each( correos, function(index, value){
		var acciones;
		acciones = "<div class='btn-tollbar' role='toolbar'>" +
					"<div class='btn-group' role='group'>" +
						"<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-editar-correo'><i class='fa fa-pencil-square-o'></i> Editar</a> " +
						"<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-eliminar-correo'><i class='fa fa-trash-o'></i> Eliminar</a> " +
					"</div>" +
				"</div>";

		var itemPreferida = "<span class='label label-warning'>No preferida</span>";

		if (value.preferida) {
			itemPreferida = "<span class='label label-info'>Preferida</span>";
		}
		
		$("#tabla-correos").dataTable().fnAddData([
			++xIndex,
			value.correo,
			value.tipocorreo.nombre,
			itemPreferida,
			acciones
		]);
	});
}

function descargarCorreos( persona ) {
	$.ajax({
		type: 		"GET",
		url: 		"api/persona/"+persona+"/correos",
		dataType: 	"json",
		data: 		{},
		success: function ( correos )
		{
			llenarCorreos(correos);
		},
		error: function ( error )
		{
			console.log(error);
		}
	});
}

function crearCorreo( e ) {
	$.ajax({
		type: 		(correoNuevo) ? "POST" : "PUT",
		url: 		(correoNuevo) ? "api/personacorreo" : "api/personacorreo/"+ $("#form-correos #updateid").val(),
		dataType: 	"json",
		data: 		$(this).serialize()+"&persona="+$("#personaid").val(),
		success: function( respuesta )
		{
			descargarCorreos($("#personaid").val());
			$("#form-correos")[0].reset();
			$("#form-correos #btn-fcor").text("Agregar Correo");
			$("#form-correos .btn-ccor").addClass("hide");
			correoNuevo = true;
			swal("Buen Trabajo!", "Registro ingresado correctamento!", "success");
		},
		error: function( error )
		{
			console.log(error);
			swal("Opps!", error.responseJSON.message, "error");
		}
	});

    if ( e != null )
		e.preventDefault();
        e.stopPropagation();
}

function editarCorreo( e ) {
	var idreg = $(e.target).closest("a").attr("idreg");
	$.ajax({
		type: 		"GET",
		url: 		"api/personacorreo/"+idreg,
		dataType: 	"json",
		data: 		{},
		success: function ( respuesta )
		{
			$("#form-correos #updateid").val(respuesta.id);
			$("#form-correos #correo").val(respuesta.correo);
			$("#form-correos #tipocorreo").val(respuesta.tipocorreo);
			$("#form-correos #preferida").val(respuesta.preferida);
			$("#form-correos .btn-fcor").text("Editar Correo");
			$("#form-correos .btn-ccor").removeClass("hide");
			correoNuevo = false;
		},
		error: function ( error )
		{
			console.log(error);
		}
	});

	if ( e != null )
		e.preventDefault();
}

function cancelarEditarCorreo( e ) {
	$("#form-correos")[0].reset();
	$("#form-correos .btn-fcor").text("Agregar Correo");
	$("#form-correos .btn-ccor").addClass("hide");
	telefonoNuevo = true;
}

function eliminarCorreo( e ) {
	var idreg = $(e.target).closest("a").attr("idreg");
	$.ajax({
		type: 		"DELETE",
		url: 		"api/personacorreo/"+idreg,
		dataType: 	"json",
		data: 		{},
		success: function ( respuesta )
		{
			descargarCorreos($("#personaid").val());
		},
		error: function ( error )
		{
			console.log(error);
		}
	});

	if ( e != null )
		e.preventDefault();
}


/** DIRECCIONES */

function llenarDirecciones( direcciones ) {
	$("#tabla-direcciones").dataTable().fnClearTable();

	var xIndex = 0;
	$.each( direcciones, function(index, value){
		var acciones;
		acciones = "<div class='btn-tollbar' role='toolbar'>" +
					"<div class='btn-group' role='group'>" +
						"<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-editar-direccion'><i class='fa fa-pencil-square-o'></i> Editar</a> " +
						"<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-eliminar-direccion'><i class='fa fa-trash-o'></i> Eliminar</a> " +
					"</div>" +
				"</div>";

		var itemPreferida = "<span class='label label-warning'>No preferida</span>";

		if (value.preferida) {
			itemPreferida = "<span class='label label-info'>Preferida</span>";
		}
		
		$("#tabla-direcciones").dataTable().fnAddData([
			++xIndex,
			value.direccion,
			value.pais.nombre,
			value.departamento.nombre,
			value.municipio.nombre,
			value.tipodireccion.nombre,
			itemPreferida,
			acciones
		]);
	});
}

function descargarDirecciones( persona ) {
	$.ajax({
		type: 		"GET",
		url: 		"api/persona/"+persona+"/direcciones",
		dataType: 	"json",
		data: 		{},
		success: function ( direcciones )
		{
			llenarDirecciones(direcciones);
		},
		error: function ( error )
		{
			console.log(error);
		}
	});
}

function crearDireccion( e ) {
	$.ajax({
		type: 		(direccionNueva) ? "POST" : "PUT",
		url: 		(direccionNueva) ? "api/personadireccion" : "api/personadireccion/"+ $("#form-direcciones #updateid").val(),
		dataType: 	"json",
		data: 		$(this).serialize()+"&persona="+$("#personaid").val(),
		success: function( respuesta )
		{
			descargarDirecciones($("#personaid").val());
			$("#form-direcciones")[0].reset();
			$("#form-direcciones #btn-fdir").text("Agregar Dirección");
			$("#form-direcciones .btn-cdir").addClass("hide");
			direccionNueva = true;
			swal("Buen Trabajo!", "Registro ingresado correctamento!", "success");
		},
		error: function( error )
		{
			console.log(error);
			swal("Opps!", error.responseJSON.message, "error");
		}
	});

    if ( e != null )
		e.preventDefault();
        e.stopPropagation();
}

function editarDireccion( e ) {
	var idreg = $(e.target).closest("a").attr("idreg");
	$.ajax({
		type: 		"GET",
		url: 		"api/personadireccion/"+idreg,
		dataType: 	"json",
		data: 		{},
		success: function ( respuesta )
		{
			$("#form-direcciones #updateid").val(respuesta.id);
			$("#form-direcciones #direccion").val(respuesta.direccion);
			$("#form-direcciones #pais").val(respuesta.pais);
			$("#form-direcciones #departamento").val(respuesta.departamento);
			$("#form-direcciones #municipio").val(respuesta.municipio);
			$("#form-direcciones #tipodireccion").val(respuesta.tipodireccion);
			$("#form-direcciones #preferida").val(respuesta.preferida);
			$("#form-direcciones .btn-fdir").text("Editar Dirección");
			$("#form-direcciones .btn-cdir").removeClass("hide");
			direccionNueva = false;
		},
		error: function ( error )
		{
			console.log(error);
		}
	});

	if ( e != null )
		e.preventDefault();
}

function cancelarEditarDireccion( e ) {
	$("#form-direcciones")[0].reset();
	$("#form-direcciones .btn-fdir").text("Agregar Direccion");
	$("#form-direcciones .btn-cdir").addClass("hide");
	telefonoNuevo = true;
}

function eliminarDireccion( e ) {
	var idreg = $(e.target).closest("a").attr("idreg");
	$.ajax({
		type: 		"DELETE",
		url: 		"api/personadireccion/"+idreg,
		dataType: 	"json",
		data: 		{},
		success: function ( respuesta )
		{
			descargarDirecciones($("#personaid").val());
		},
		error: function ( error )
		{
			console.log(error);
		}
	});

	if ( e != null )
		e.preventDefault();
}

function bloquearTabs() {
    $(".lidac").addClass("disabled");
    $(".lidir").addClass("disabled");
    $(".litel").addClass("disabled");
    $(".licor").addClass("disabled");

    $(".tabdac").removeAttr("data-toggle");
    $(".tabdir").removeAttr("data-toggle");
    $(".tabtel").removeAttr("data-toggle");
    $(".tabcor").removeAttr("data-toggle");

    $(".tabdac").removeAttr("href");
    $(".tabdir").removeAttr("href");
    $(".tabtel").removeAttr("href");
    $(".tabcor").removeAttr("href");
}

function habilitarTabs() {
    $(".lidac").removeClass("disabled");
    $(".lidir").removeClass("disabled");
    $(".litel").removeClass("disabled");
    $(".licor").removeClass("disabled");
    
    $(".tabdac").attr("data-toggle", "tab");
    $(".tabdir").attr("data-toggle", "tab");
    $(".tabtel").attr("data-toggle", "tab");
    $(".tabcor").attr("data-toggle", "tab");
    
    $(".tabdac").attr("href", "#datosaccesos");
    $(".tabdir").attr("href", "#direcciones");
    $(".tabtel").attr("href", "#telefonos");
    $(".tabcor").attr("href", "#correos");
    
}

function llenarTabla( e ) 
{
	$.ajax({
		type:'GET',
		url:'api/usuario',
		dataType:'json',
		data:{},
		success:function( registros )
		{
			
			$("#tabla-registros").dataTable().fnClearTable();

			var xIndex = 0;
			$.each( registros, function(index, value){
				var acciones;
                acciones = "<div class='btn-tollbar' role='toolbar'>" +
                            "<div class='btn-group' role='group'>" +
                                "<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-editar'><i class='fa fa-pencil-square-o'></i> Editar</a> " +
                            "</div>" +
                        "</div>";
            
				
				$("#tabla-registros").dataTable().fnAddData([
					++xIndex,
					value.persona.nombres,
                    value.persona.apellidos,
					acciones
				]);
			});
			
		},
		error: function( error )
		{
			console.log(error);
		}
	});
	

	if ( e != null )
		e.preventDefault();
}


function llenarProfesiones( e ) {
    $.ajax({
		type:'GET',
		url:'api/tipoprofesion',
		dataType:'json',
		data:{},
		success:function( registros )
		{
			var optionTipoProfesion = $("#form-datosgenerales #profesion");
            optionTipoProfesion.find('option').remove();
            $.each(registros, function(index, value){
                optionTipoProfesion.append($("<option />").val(value.id).text(value.nombre));
            });
		},
		error: function( error )
		{
			console.log(error);
		}
	});
	

	if ( e != null )
		e.preventDefault();
}

function llenarTipoDireccion( e ) {
    $.ajax({
		type:'GET',
		url:'api/tipodireccion',
		dataType:'json',
		data:{},
		success:function( registros )
		{
			var optionTipoProfesion = $("#form-direcciones #tipodireccion");
            optionTipoProfesion.find('option').remove();
            $.each(registros, function(index, value){
                optionTipoProfesion.append($("<option />").val(value.id).text(value.nombre));
            });
		},
		error: function( error )
		{
			console.log(error);
		}
	});
	

	if ( e != null )
		e.preventDefault();
}

function llenarTipoTelefono( e ) {
    $.ajax({
		type:'GET',
		url:'api/tipotelefono',
		dataType:'json',
		data:{},
		success:function( registros )
		{
			var optionTipoProfesion = $("#form-telefonos #tipotelefono");
            optionTipoProfesion.find('option').remove();
            $.each(registros, function(index, value){
                optionTipoProfesion.append($("<option />").val(value.id).text(value.nombre));
            });
		},
		error: function( error )
		{
			console.log(error);
		}
	});
	

	if ( e != null )
		e.preventDefault();
}

function llenarTipoCorreo( e ) {
    $.ajax({
		type:'GET',
		url:'api/tipocorreo',
		dataType:'json',
		data:{},
		success:function( registros )
		{
			var optionTipoProfesion = $("#form-correos #tipocorreo");
            optionTipoProfesion.find('option').remove();
            $.each(registros, function(index, value){
                optionTipoProfesion.append($("<option />").val(value.id).text(value.nombre));
            });
		},
		error: function( error )
		{
			console.log(error);
		}
	});
	

	if ( e != null )
		e.preventDefault();
}

function llenarPais( e ) {
	$.ajax({
		type:'GET',
		url:'api/paises',
		dataType:'json',
		data:{},
		success:function( registros )
		{
			var optionTipoProfesion = $("#form-direcciones #pais");
            optionTipoProfesion.find('option').remove();
            $.each(registros, function(index, value){
                optionTipoProfesion.append($("<option />").val(value.id).text(value.nombre));
            });
			llenarDepartamentos();
		},
		error: function( error )
		{
			console.log(error);
		}
	});

	if ( e != null )
		e.preventDefault();
}

function llenarDepartamentos( e ) {
	$.ajax({
		type:'GET',
		url:'api/paises/'+$("#form-direcciones #pais").val()+"/departamentos",
		dataType:'json',
		data:{},
		success:function( registros )
		{
			var optionTipoProfesion = $("#form-direcciones #departamento");
            optionTipoProfesion.find('option').remove();
			if (registros.length > 0) {
				$.each(registros, function(index, value){
					optionTipoProfesion.append($("<option />").val(value.id).text(value.nombre));
				});
			} else {
				optionTipoProfesion.append($("<option />").val(0).text("Sin Seleccionar"));
			}
			llenarMunicipios();
		},
		error: function( error )
		{
			console.log(error);
		}
	});
	
	if ( e != null )
		e.preventDefault();
}

function llenarMunicipios( e ) {
	$.ajax({
		type:'GET',
		url:'api/departamentos/'+$("#form-direcciones #departamento").val()+"/municipios",
		dataType:'json',
		data:{},
		success:function( registros )
		{
			var optionTipoProfesion = $("#form-direcciones #municipio");
            optionTipoProfesion.find('option').remove();
            if (registros.length > 0) {
				$.each(registros, function(index, value){
					optionTipoProfesion.append($("<option />").val(value.id).text(value.nombre));
				});
			} else {
				optionTipoProfesion.append($("<option />").val(0).text("Sin Seleccionar"));
			}
		},
		error: function( error )
		{
			console.log(error);
		}
	});
	
	if ( e != null )
		e.preventDefault();
}

function closeBlockUsuario( e ) {
	$("#block-usuario").addClass("hide");
	if ( e != null )
		e.preventDefault();
}
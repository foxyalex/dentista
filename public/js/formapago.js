$(document).ready(iniciar);

function iniciar() {

	$(".btn-crear").on("click", mostrarModalCrear);
	$("#form-crear").on("submit", crearRegistro);
	$("#form-editar").on("submit", editarRegistro);
	$("#tabla-registros").delegate(".btn-editar", "click", verModalEditar);
	$("#tabla-registros").delegate(".btn-alta", "click", darAlta);
	$("#tabla-registros").delegate(".btn-baja", "click", darBaja);
	llenarTabla();

    $('#tabla-registros').DataTable({
		responsive: true,
		"oLanguage": {
			"sLengthMenu": "Mostrando _MENU_ filas",
		  	"sSearch": "",
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
		  			"sFirst":    "Primero",
		  			"sLast":     "Último",
		  			"sNext":     "Siguiente",
		  			"sPrevious": "Anterior"
				}
		}
    });
}

function llenarTabla( e ) 
{
	$.ajax({
		type:'GET',
		url:'api/formapago',
		dataType:'json',
		data:{},
		success:function( registros )
		{
			
			$("#tabla-registros").dataTable().fnClearTable();

			var xIndex = 0;
			$.each( registros, function(index, value){
				var acciones;


				if(value.estado) {
					acciones = "<div class='btn-tollbar' role='toolbar'>" +
								"<div class='btn-group' role='group'>" +
									"<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-editar'><i class='fa fa-pencil-square-o'></i> Editar</a> " +
									"<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-baja'><i class='fa fa-thumbs-down'></i> Desactivar</a> " +
								"</div>" +
							"</div>";
				} else {
					acciones = "<div class='btn-tollbar' role='toolbar'>" +
								"<div class='btn-group' role='group'>" +
									"<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-editar'><i class='fa fa-pencil-square-o'></i> Editar</a> " +
									"<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-alta'><i class='fa fa-thumbs-up'></i> Activar</a> " +
								"</div>" +
							"</div>";
				}
				
				$("#tabla-registros").dataTable().fnAddData([
					++xIndex,
					value.nombre,
                    (value.sedeposita ? "Se Deposita" : "No se Deposita"),
					acciones
				]);
			});
			
		},
		error: function( error )
		{
			console.log(error);
		}
	});
	
	if ( e != null )
		e.preventDefault();
}

function mostrarModalCrear( e ) {

	$("#modal-crear").modal('show');

	if(e!=null)
		e.preventDefault();
}

function crearRegistro( e )
{
    var sedeposita = 0;
	if($("#form-crear #sedeposita").is(":checked")) {
        sedeposita = 1;
    }
	$.ajax({
		type: 		"POST",
		url: 		"api/formapago",
		dataType: 	"json",
		data: 		$(this).serialize()+"&sedeposita="+sedeposita,
		success: function( respuesta )
		{
			$("#modal-crear").modal("hide");
			swal("Buen Trabajo!", "Registro creado correctamento!", "success");
			setTimeout( function(){ ratPack.refresh(); }, 300 );
		},
		error: function( error )
		{
			console.log(error);
			swal("Opps!", error.responseJSON.message, "error");
		}
	});

	if ( e != null )
		e.preventDefault();
		e.stopPropagation();
}

function verModalEditar( e )
{
	var idreg = $(e.target).closest("a").attr("idreg");
	$.ajax({
		type: 		"GET",
		url: 		"api/formapago/"+idreg,
		dataType: 	"json",
		data: 		{},
		success: function ( registro )
		{
			$("#modal-editar").modal("show");
			$("#modal-editar #form-editar #nombre").val(registro.nombre);
            if(registro.sedeposita) {
                $("#modal-editar #form-editar #sedeposita").prop('checked', true);
            } else {
                $("#modal-editar #form-editar #sedeposita").prop('checked', false);
            }
			$("#modal-editar #form-editar #updateid").val(registro.id);
		},
		error: function ( error )
		{
			console.log(error);
		}
	});

	if ( e != null)
		e.preventDefault();
}

function editarRegistro( e )
{
	var idreg = $("#modal-editar #form-editar #updateid").val();
    var sedeposita = 0;
	if($("#form-editar #sedeposita").is(":checked")) {
        sedeposita = 1;
    }
	$.ajax({
		type: 		"PUT",
		url: 		"api/formapago/"+idreg,
		dataType: 	"json",
		data: 		$(this).serialize()+"&sedeposita="+sedeposita,
		success: function( registro )
		{
			$("#modal-editar").modal("hide");
			swal("Buen Trabajo!", "Registro actualizado correctamento!", "success");
			setTimeout( function(){ ratPack.refresh(); }, 300 );
			
		},
		error: function( error )
		{
			console.log(error);
			swal("Opps!", error.responseJSON.message, "error");
		}
	});

	if ( e != null )
		e.preventDefault();
		e.stopPropagation();
}

function darAlta( e ) {
	var idreg = $(e.target).closest("a").attr("idreg");
	$.ajax({
		type: 		"PUT",
		url: 		"api/formapago/"+idreg,
		dataType: 	"json",
		data: 		{estado: 1},
		success: function ( registro )
		{
			setTimeout( function(){ ratPack.refresh(); }, 300 );
		},
		error: function ( error )
		{
			console.log(error);
		}
	});

	if ( e != null)
		e.preventDefault();
}

function darBaja( e ) {
	var idreg = $(e.target).closest("a").attr("idreg");
	$.ajax({
		type: 		"PUT",
		url: 		"api/formapago/"+idreg,
		dataType: 	"json",
		data: 		{estado: 0},
		success: function ( registro )
		{
			setTimeout( function(){ ratPack.refresh(); }, 300 );
		},
		error: function ( error )
		{
			console.log(error);
		}
	});

	if ( e != null)
		e.preventDefault();
}
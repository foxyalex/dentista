$(document).ready(iniciar);

var esNuevo = true;
var telefonoNuevo = true;
var correoNuevo = true;
var direccionNueva = true;
var facturacionNueva = true;

function iniciar() {

    $(".btn-crear").on("click", verBloquePaciente);
    $(".close-paciente").on("click", closeBlockPaciente);
    $("#form-datosgenerales").on("submit", guardarPersona);
	$("#form-telefonos").on("submit", crearTelefono);
	$("#form-correos").on("submit", crearCorreo);
	$("#form-direcciones").on("submit", crearDireccion);
	$("#form-facturacion").on("submit", crearFacturacion);
	$("#form-datospaciente").on("submit", actualizarDatosPaciente);
	$("#form-historiamedica").on("submit", actualizarHistoriaMedica);
	$("#form-historiaodontologica").on("submit", actualizarHistoriaOdontologica);
	$("#form-enfermedades").on("submit", crearEnfermedad);
	$("#tabla-registros").delegate(".btn-editar", "click", verBloquePacienteEditar);
	$("#tabla-telefonos").delegate(".btn-editar-telefono", "click", editarTelefono);
	$("#tabla-telefonos").delegate(".btn-eliminar-telefono", "click", eliminarTelefono);
	$("#tabla-correos").delegate(".btn-editar-correo", "click", editarCorreo);
	$("#tabla-correos").delegate(".btn-eliminar-correo", "click", eliminarCorreo);
	$("#tabla-direcciones").delegate(".btn-editar-direccion", "click", editarDireccion);
	$("#tabla-direcciones").delegate(".btn-eliminar-direccion", "click", eliminarDireccion);
	$("#tabla-facturacion").delegate(".btn-editar-facturacion", "click", editarFacturacion);
	$("#tabla-facturacion").delegate(".btn-eliminar-facturacion", "click", eliminarFacturacion);
	$("#tabla-enfermedades").delegate(".btn-eliminar-enfermedad", "click", eliminarEnfermedad);
	$("#form-direcciones #pais").on("change", llenarDepartamentos);
	$("#form-direcciones #departamento").on("change", llenarMunicipios);

	$(".btn-ctel").on("click", cancelarEditarTelefono);
	$(".btn-cdir").on("click", cancelarEditarDireccion);
	$(".btn-ccor").on("click", cancelarEditarCorreo);
	$(".btn-cfac").on("click", cancelarEditarFacturacion);

    llenarTabla();
    llenarProfesiones();
    llenarTipoDireccion();
    llenarTipoTelefono();
    llenarTipoCorreo();
	llenarPais();
	llenarMotivoConsulta();
	llenarCentroEmergencia();
	llenarTipoPaciente();
	llenarTipoSangre();
	llenarMedioPublicitario();
	llenarTipoEnfermedad();

    $('.mydatepicker').datepicker();
    
    $('#tabla-registros, #tabla-direcciones, #tabla-telefonos, #tabla-correos, #tabla-facturacion, #tabla-enfermedades').DataTable({
		responsive: true,
		"oLanguage": {
			"sLengthMenu": "Mostrando _MENU_ filas",
		  	"sSearch": "",
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
		  			"sFirst":    "Primero",
		  			"sLast":     "Último",
		  			"sNext":     "Siguiente",
		  			"sPrevious": "Anterior"
				}
		}
    });
}

function verBloquePaciente( e ) {

    esNuevo = true;
    $("#block-paciente").removeClass('hide');
	$("#form-datosgenerales")[0].reset();
	$("#btn-fdg").text("Crear Paciente");
    bloquearTabs();

    if ( e != null )
		e.preventDefault();
}

function guardarPersona( e ) {

    $.ajax({
		type: 		(esNuevo) ? "POST" : "PUT",
		url: 		(esNuevo) ? "api/persona" : "api/persona/"+ $("#personaid").val(),
		dataType: 	"json",
		data: 		$(this).serialize(),
		success: function( respuesta )
		{
			if ( esNuevo ) {
				$("#personaid").val(respuesta.id);
            	crearPaciente(respuesta);
				swal("Buen Trabajo!", "Registro ingresado correctamento!", "success");
			} else {
				llenarTabla();
				swal("Buen Trabajo!", "Registro actualizado correctamento!", "success");
			}
		},
		error: function( error )
		{
			swal("Opps!", error.responseJSON.message, "error");
			console.log(error)
		}
	});

    if ( e != null )
		e.preventDefault();
        e.stopPropagation();
}

function crearPaciente( persona ) {
	$.ajax({
		type: 		"POST",
		url: 		"api/paciente",
		dataType: 	"json",
		data: 		{persona: persona.id},
		success: function( respuesta )
		{
            $("#pacienteid").val(respuesta.id);
			llenarTabla();
            habilitarTabs();
		},
		error: function( error )
		{
			console.log(error)
		}
	});
}

function actualizarDatosPaciente( e ) {
	$.ajax({
		type: 		"PUT",
		url: 		"api/paciente/"+ $("#personaid").val(),
		dataType: 	"json",
		data: 		$(this).serialize(),
		success: function( respuesta )
		{
			swal("Buen Trabajo!", "Registro actualizado correctamento!", "success");
		},
		error: function( error )
		{
			console.log(error);
			swal("Opps!", error.responseJSON.message, "error");
		}
	});

    if ( e != null )
		e.preventDefault();
        e.stopPropagation();
}

function actualizarHistoriaMedica( e ) {
	$.ajax({
		type: 		"PUT",
		url: 		"api/paciente/"+ $("#personaid").val(),
		dataType: 	"json",
		data: 		$(this).serialize(),
		success: function( respuesta )
		{
			swal("Buen Trabajo!", "Registro actualizado correctamento!", "success");
		},
		error: function( error )
		{
			console.log(error);
			swal("Opps!", error.responseJSON.message, "error");
		}
	});

    if ( e != null )
		e.preventDefault();
        e.stopPropagation();
}

function actualizarHistoriaOdontologica( e ) {
	console.log($(this).serialize());
	$.ajax({
		type: 		"PUT",
		url: 		"api/paciente/"+ $("#personaid").val(),
		dataType: 	"json",
		data: 		$(this).serialize(),
		success: function( respuesta )
		{
			swal("Buen Trabajo!", "Registro actualizado correctamento!", "success");
		},
		error: function( error )
		{
			console.log(error);
			swal("Opps!", error.responseJSON.message, "error");
		}
	});

    if ( e != null )
		e.preventDefault();
        e.stopPropagation();
}

function verBloquePacienteEditar( e ) {
	var idreg = $(e.target).closest("a").attr("idreg");
	$.ajax({
		type: 		"GET",
		url: 		"api/paciente/"+idreg,
		dataType: 	"json",
		data: 		{},
		success: function ( paciente )
		{
			esNuevo = false;
			$("#block-paciente").removeClass('hide');
			$("#btn-fdg").text("Actualizar Paciente");
			$("#pacienteid").val(paciente.id);
			$("#personaid").val(paciente.persona);
			habilitarTabs();
			llenarInformaciónPaciente(paciente);
		},
		error: function ( error )
		{
			console.log(error);
		}
	});

	if ( e != null)
		e.preventDefault();
}

function llenarInformaciónPaciente( paciente ) {
	$("#form-datosgenerales #nombres").val(paciente.persona_completa.nombres);
	$("#form-datosgenerales #apellidos").val(paciente.persona_completa.apellidos);
	$("#form-datosgenerales #casada").val(paciente.persona_completa.casada);
	$("#form-datosgenerales #dni").val(paciente.persona_completa.dni);
	$("#form-datosgenerales #nacimiento").val(paciente.persona_completa.nacimiento);
	$("#form-datosgenerales #profesion").val(paciente.persona_completa.profesion);
	$("#form-datosgenerales #genero").val(paciente.persona_completa.genero);
	$("#form-datosgenerales #estadocivil").val(paciente.persona_completa.estadocivil);
	
	$("#form-datospaciente #motivoconsulta").val(paciente.motivoconsulta);
	$("#form-datospaciente #centroemergencia").val(paciente.centroemergencia);
	$("#form-datospaciente #aspectosrelevantes").val(paciente.aspectosrelevantes);
	$("#form-datospaciente #tipopaciente").val(paciente.tipopaciente);
	$("#form-datospaciente #tiposangre").val(paciente.tiposangre);
	$("#form-datospaciente #polizaseguro").val(paciente.polizaseguro);
	$("#form-datospaciente #mediopublicitario").val(paciente.mediopublicitario);
	$("#form-datospaciente #montoautorizado").val(paciente.montoautorizado);


	$("#form-historiamedica #hmproblemas").val(paciente.hmproblemas);
	$("#form-historiamedica #hmcirugias").val(paciente.hmcirugias);
	$("#form-historiamedica #hmtrastornocorazon").val(paciente.hmtrastornocorazon);
	$("#form-historiamedica #hmtrastornocorazoncomentario").val(paciente.hmtrastornocorazoncomentario);
	$("#form-historiamedica #hmmedicamentos").val(paciente.hmmedicamentos);
	$("#form-historiamedica #hmfuma").val(paciente.hmfuma);
	$("#form-historiamedica #hmbebidas").val(paciente.hmbebidas);
	$("#form-historiamedica #hmtratamiento").val(paciente.hmtratamiento);
	$("#form-historiamedica #hmalergico").val(paciente.hmalergico);
	$("#form-historiamedica #hmaccidentes").val(paciente.hmaccidentes);
	$("#form-historiamedica #hmcomentarios").val(paciente.hmcomentarios);

	$("#form-historiaodontologica #houltimavisita").val(paciente.houltimavisita);
	$("#form-historiaodontologica #hotratamientodental").val(paciente.hotratamientodental);
	$("#form-historiaodontologica #hocomentariosrelevantes").val(paciente.hocomentariosrelevantes);
	$("#form-historiaodontologica #homolestiaactual").val(paciente.homolestiaactual);
	$("#form-historiaodontologica #hoaccidentes").val(paciente.hoaccidentes);
	$("#form-historiaodontologica #hoextraccion").val(paciente.hoextraccion);

	llenarTelefonos(paciente.persona_completa.telefonos);
	llenarCorreos(paciente.persona_completa.correos);
	llenarDirecciones(paciente.persona_completa.direcciones);
	llenarFacturacion(paciente.persona_completa.facturaciones);
	llenarEnfermedades(paciente.enfermedades);
}

/** TELEFONOS */

function llenarTelefonos( telefonos ) {
	$("#tabla-telefonos").dataTable().fnClearTable();

	var xIndex = 0;
	$.each( telefonos, function(index, value){
		var acciones;
		acciones = "<div class='btn-tollbar' role='toolbar'>" +
					"<div class='btn-group' role='group'>" +
						"<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-editar-telefono'><i class='fa fa-pencil-square-o'></i> Editar</a> " +
						"<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-eliminar-telefono'><i class='fa fa-trash-o'></i> Eliminar</a> " +
					"</div>" +
				"</div>";

		var itemPreferida = "<span class='label label-warning'>No preferida</span>";

		if (value.preferida) {
			itemPreferida = "<span class='label label-info'>Preferida</span>";
		}
		
		$("#tabla-telefonos").dataTable().fnAddData([
			++xIndex,
			value.numero,
			value.tipotelefono.nombre,
			itemPreferida,
			acciones
		]);
	});
}

function descargarTelefonos( persona ) {
	$.ajax({
		type: 		"GET",
		url: 		"api/persona/"+persona+"/telefonos",
		dataType: 	"json",
		data: 		{},
		success: function ( telefonos )
		{
			llenarTelefonos(telefonos);
		},
		error: function ( error )
		{
			console.log(error);
		}
	});
}

function crearTelefono( e ) {
	$.ajax({
		type: 		(telefonoNuevo) ? "POST" : "PUT",
		url: 		(telefonoNuevo) ? "api/personatelefono" : "api/personatelefono/"+ $("#form-telefonos #updateid").val(),
		dataType: 	"json",
		data: 		$(this).serialize()+"&persona="+$("#personaid").val(),
		success: function( respuesta )
		{
			descargarTelefonos($("#personaid").val());
			$("#form-telefonos")[0].reset();
			$("#form-telefonos .btn-ftel").text("Agregar Telefono");
			$("#form-telefonos .btn-ctel").addClass("hide");
			telefonoNuevo = true;
			swal("Buen Trabajo!", "Registro ingresado correctamento!", "success");
		},
		error: function( error )
		{
			console.log(error)
			swal("Opps!", error.responseJSON.message, "error");
		}
	});

    if ( e != null )
		e.preventDefault();
        e.stopPropagation();
}

function editarTelefono( e ) {
	var idreg = $(e.target).closest("a").attr("idreg");
	$.ajax({
		type: 		"GET",
		url: 		"api/personatelefono/"+idreg,
		dataType: 	"json",
		data: 		{},
		success: function ( respuesta )
		{
			$("#form-telefonos #updateid").val(respuesta.id);
			$("#form-telefonos #numero").val(respuesta.numero);
			$("#form-telefonos #tipotelefono").val(respuesta.tipotelefono);
			$("#form-telefonos #preferida").val(respuesta.preferida);
			$("#form-telefonos .btn-ftel").text("Editar Telefono");
			$("#form-telefonos .btn-ctel").removeClass("hide");
			telefonoNuevo = false;
		},
		error: function ( error )
		{
			console.log(error);
			swal("Opps!", error.responseJSON.message, "error");
		}
	});

	if ( e != null )
		e.preventDefault();
}

function cancelarEditarTelefono( e ) {
	$("#form-telefonos")[0].reset();
	$("#form-telefonos .btn-ftel").text("Agregar Telefono");
	$("#form-telefonos .btn-ctel").addClass("hide");
	telefonoNuevo = true;
}

function eliminarTelefono( e ) {
	var idreg = $(e.target).closest("a").attr("idreg");
	swal({
		title: "Estas Seguro?",
		text: "El registro no se podra recuperar!",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Si, elimarlo!",
		cancelButtonText: "Cancelar",
		closeOnConfirm: false
	},
	function(){
		$.ajax({
			type: 		"DELETE",
			url: 		"api/personatelefono/"+idreg,
			dataType: 	"json",
			data: 		{},
			success: function ( respuesta )
			{
				swal("Eliminado!", "El Registro se elimino correctamente.", "success");
				descargarTelefonos($("#personaid").val());
			},
			error: function ( error )
			{
				console.log(error);
				swal("Opps!", error.responseJSON.message, "error");
			}
		});
	});
	
	if ( e != null )
		e.preventDefault();
}

/** CORREOS */

function llenarCorreos( correos ) {
	$("#tabla-correos").dataTable().fnClearTable();

	var xIndex = 0;
	$.each( correos, function(index, value){
		var acciones;
		acciones = "<div class='btn-tollbar' role='toolbar'>" +
					"<div class='btn-group' role='group'>" +
						"<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-editar-correo'><i class='fa fa-pencil-square-o'></i> Editar</a> " +
						"<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-eliminar-correo'><i class='fa fa-trash-o'></i> Eliminar</a> " +
					"</div>" +
				"</div>";

		var itemPreferida = "<span class='label label-warning'>No preferida</span>";

		if (value.preferida) {
			itemPreferida = "<span class='label label-info'>Preferida</span>";
		}
		
		$("#tabla-correos").dataTable().fnAddData([
			++xIndex,
			value.correo,
			value.tipocorreo.nombre,
			itemPreferida,
			acciones
		]);
	});
}

function descargarCorreos( persona ) {
	$.ajax({
		type: 		"GET",
		url: 		"api/persona/"+persona+"/correos",
		dataType: 	"json",
		data: 		{},
		success: function ( correos )
		{
			llenarCorreos(correos);
		},
		error: function ( error )
		{
			console.log(error);
		}
	});
}

function crearCorreo( e ) {
	$.ajax({
		type: 		(correoNuevo) ? "POST" : "PUT",
		url: 		(correoNuevo) ? "api/personacorreo" : "api/personacorreo/"+ $("#form-correos #updateid").val(),
		dataType: 	"json",
		data: 		$(this).serialize()+"&persona="+$("#personaid").val(),
		success: function( respuesta )
		{
			descargarCorreos($("#personaid").val());
			$("#form-correos")[0].reset();
			$("#form-correos #btn-fcor").text("Agregar Correo");
			$("#form-correos .btn-ccor").addClass("hide");
			correoNuevo = true;
			swal("Buen Trabajo!", "Registro ingresado correctamento!", "success");
		},
		error: function( error )
		{
			console.log(error);
			swal("Opps!", error.responseJSON.message, "error");
		}
	});

    if ( e != null )
		e.preventDefault();
        e.stopPropagation();
}

function editarCorreo( e ) {
	var idreg = $(e.target).closest("a").attr("idreg");
	$.ajax({
		type: 		"GET",
		url: 		"api/personacorreo/"+idreg,
		dataType: 	"json",
		data: 		{},
		success: function ( respuesta )
		{
			$("#form-correos #updateid").val(respuesta.id);
			$("#form-correos #correo").val(respuesta.correo);
			$("#form-correos #tipocorreo").val(respuesta.tipocorreo);
			$("#form-correos #preferida").val(respuesta.preferida);
			$("#form-correos .btn-fcor").text("Editar Correo");
			$("#form-correos .btn-ccor").removeClass("hide");
			correoNuevo = false;
		},
		error: function ( error )
		{
			console.log(error);
			swal("Opps!", error.responseJSON.message, "error");
		}
	});

	if ( e != null )
		e.preventDefault();
}

function cancelarEditarCorreo( e ) {
	$("#form-correos")[0].reset();
	$("#form-correos #btn-fcor").text("Agregar Correo");
	$("#form-correos .btn-ccor").addClass("hide");
	telefonoNuevo = true;
}

function eliminarCorreo( e ) {
	var idreg = $(e.target).closest("a").attr("idreg");
	swal({
		title: "Estas Seguro?",
		text: "El registro no se podra recuperar!",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Si, elimarlo!",
		cancelButtonText: "Cancelar",
		closeOnConfirm: false
	},
	function(){
		$.ajax({
			type: 		"DELETE",
			url: 		"api/personacorreo/"+idreg,
			dataType: 	"json",
			data: 		{},
			success: function ( respuesta )
			{
				swal("Eliminado!", "El Registro se elimino correctamente.", "success");
				descargarCorreos($("#personaid").val());
			},
			error: function ( error )
			{
				console.log(error);
				swal("Opps!", error.responseJSON.message, "error");
			}
		});
	});

	if ( e != null )
		e.preventDefault();
}


/** DIRECCIONES */

function llenarDirecciones( direcciones ) {
	$("#tabla-direcciones").dataTable().fnClearTable();

	var xIndex = 0;
	$.each( direcciones, function(index, value){
		var acciones;
		acciones = "<div class='btn-tollbar' role='toolbar'>" +
					"<div class='btn-group' role='group'>" +
						"<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-editar-direccion'><i class='fa fa-pencil-square-o'></i> Editar</a> " +
						"<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-eliminar-direccion'><i class='fa fa-trash-o'></i> Eliminar</a> " +
					"</div>" +
				"</div>";

		var itemPreferida = "<span class='label label-warning'>No preferida</span>";

		if (value.preferida) {
			itemPreferida = "<span class='label label-info'>Preferida</span>";
		}
		
		$("#tabla-direcciones").dataTable().fnAddData([
			++xIndex,
			value.direccion,
			value.pais.nombre,
			value.departamento.nombre,
			value.municipio.nombre,
			value.tipodireccion.nombre,
			itemPreferida,
			acciones
		]);
	});
}

function descargarDirecciones( persona ) {
	$.ajax({
		type: 		"GET",
		url: 		"api/persona/"+persona+"/direcciones",
		dataType: 	"json",
		data: 		{},
		success: function ( direcciones )
		{
			llenarDirecciones(direcciones);
		},
		error: function ( error )
		{
			console.log(error);
		}
	});
}

function crearDireccion( e ) {
	$.ajax({
		type: 		(direccionNueva) ? "POST" : "PUT",
		url: 		(direccionNueva) ? "api/personadireccion" : "api/personadireccion/"+ $("#form-direcciones #updateid").val(),
		dataType: 	"json",
		data: 		$(this).serialize()+"&persona="+$("#personaid").val(),
		success: function( respuesta )
		{
			descargarDirecciones($("#personaid").val());
			$("#form-direcciones")[0].reset();
			$("#form-direcciones #btn-fdir").text("Agregar Dirección");
			$("#form-direcciones .btn-cdir").addClass("hide");
			direccionNueva = true;
			swal("Buen Trabajo!", "Registro ingresado correctamento!", "success");
		},
		error: function( error )
		{
			console.log(error);
			swal("Opps!", error.responseJSON.message, "error");
		}
	});

    if ( e != null )
		e.preventDefault();
        e.stopPropagation();
}

function editarDireccion( e ) {
	var idreg = $(e.target).closest("a").attr("idreg");
	$.ajax({
		type: 		"GET",
		url: 		"api/personadireccion/"+idreg,
		dataType: 	"json",
		data: 		{},
		success: function ( respuesta )
		{
			$("#form-direcciones #updateid").val(respuesta.id);
			$("#form-direcciones #direccion").val(respuesta.direccion);
			$("#form-direcciones #pais").val(respuesta.pais);
			$("#form-direcciones #departamento").val(respuesta.departamento);
			$("#form-direcciones #municipio").val(respuesta.municipio);
			$("#form-direcciones #tipodireccion").val(respuesta.tipodireccion);
			$("#form-direcciones #preferida").val(respuesta.preferida);
			$("#form-direcciones .btn-fdir").text("Editar Dirección");
			$("#form-direcciones .btn-cdir").removeClass("hide");
			direccionNueva = false;
		},
		error: function ( error )
		{
			console.log(error);
			swal("Opps!", error.responseJSON.message, "error");
		}
	});

	if ( e != null )
		e.preventDefault();
}

function cancelarEditarDireccion( e ) {
	$("#form-direcciones")[0].reset();
	$("#form-direcciones #btn-fdir").text("Agregar Direccion");
	$("#form-direcciones .btn-cdir").addClass("hide");
	telefonoNuevo = true;
}

function eliminarDireccion( e ) {
	var idreg = $(e.target).closest("a").attr("idreg");
	swal({
		title: "Estas Seguro?",
		text: "El registro no se podra recuperar!",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Si, elimarlo!",
		cancelButtonText: "Cancelar",
		closeOnConfirm: false
	},
	function(){
		$.ajax({
			type: 		"DELETE",
			url: 		"api/personadireccion/"+idreg,
			dataType: 	"json",
			data: 		{},
			success: function ( respuesta )
			{
				swal("Eliminado!", "El Registro se elimino correctamente.", "success");
				descargarDirecciones($("#personaid").val());
			},
			error: function ( error )
			{
				console.log(error);
				swal("Opps!", error.responseJSON.message, "error");
			}
		});
	});
	
	if ( e != null )
		e.preventDefault();
}

/** FACTURACION */

function llenarFacturacion( facturacion ) {
	$("#tabla-facturacion").dataTable().fnClearTable();

	var xIndex = 0;
	$.each( facturacion, function(index, value){
		var acciones;
		acciones = "<div class='btn-tollbar' role='toolbar'>" +
					"<div class='btn-group' role='group'>" +
						"<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-editar-facturacion'><i class='fa fa-pencil-square-o'></i> Editar</a> " +
						"<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-eliminar-facturacion'><i class='fa fa-trash-o'></i> Eliminar</a> " +
					"</div>" +
				"</div>";

		var itemPreferida = "<span class='label label-warning'>No preferida</span>";

		if (value.preferida) {
			itemPreferida = "<span class='label label-info'>Preferida</span>";
		}
		
		$("#tabla-facturacion").dataTable().fnAddData([
			++xIndex,
			value.nombre,
			value.direccion,
			value.nit,
			itemPreferida,
			acciones
		]);
	});
}

function descargarFacturacion( persona ) {
	$.ajax({
		type: 		"GET",
		url: 		"api/persona/"+persona+"/facturacion",
		dataType: 	"json",
		data: 		{},
		success: function ( facturacion )
		{
			llenarFacturacion(facturacion);
		},
		error: function ( error )
		{
			console.log(error);
		}
	});
}

function crearFacturacion( e ) {
	$.ajax({
		type: 		(facturacionNueva) ? "POST" : "PUT",
		url: 		(facturacionNueva) ? "api/personafacturacion" : "api/personafacturacion/"+ $("#form-facturacion #updateid").val(),
		dataType: 	"json",
		data: 		$(this).serialize()+"&persona="+$("#personaid").val(),
		success: function( respuesta )
		{
			descargarFacturacion($("#personaid").val());
			$("#form-facturacion")[0].reset();
			$("#form-facturacion #btn-ffac").text("Agregar Facturación");
			$("#form-facturacion .btn-cfac").addClass("hide");
			facturacionNueva = true;
			swal("Buen Trabajo!", "Registro ingresado correctamento!", "success");
		},
		error: function( error )
		{
			console.log(error);
			swal("Opps!", error.responseJSON.message, "error");
		}
	});

    if ( e != null )
		e.preventDefault();
        e.stopPropagation();
}

function editarFacturacion( e ) {
	var idreg = $(e.target).closest("a").attr("idreg");
	$.ajax({
		type: 		"GET",
		url: 		"api/personafacturacion/"+idreg,
		dataType: 	"json",
		data: 		{},
		success: function ( respuesta )
		{
			$("#form-facturacion #updateid").val(respuesta.id);
			$("#form-facturacion #nombre").val(respuesta.nombre);
			$("#form-facturacion #direccion").val(respuesta.direccion);
			$("#form-facturacion #nit").val(respuesta.nit);
			$("#form-facturacion #preferida").val(respuesta.preferida);
			$("#form-facturacion .btn-ffac").text("Editar Facturación");
			$("#form-facturacion .btn-cfac").removeClass("hide");
			facturacionNueva = false;
		},
		error: function ( error )
		{
			console.log(error);
			swal("Opps!", error.responseJSON.message, "error");
		}
	});

	if ( e != null )
		e.preventDefault();
}

function cancelarEditarFacturacion( e ) {
	$("#form-facturacion")[0].reset();
	$("#form-facturacion #btn-ffac").text("Agregar Facturacion");
	$("#form-facturacion .btn-cfac").addClass("hide");
	telefonoNuevo = true;
}

function eliminarFacturacion( e ) {
	var idreg = $(e.target).closest("a").attr("idreg");
	swal({
		title: "Estas Seguro?",
		text: "El registro no se podra recuperar!",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Si, elimarlo!",
		cancelButtonText: "Cancelar",
		closeOnConfirm: false
	},
	function(){
		$.ajax({
			type: 		"DELETE",
			url: 		"api/personafacturacion/"+idreg,
			dataType: 	"json",
			data: 		{},
			success: function ( respuesta )
			{
				swal("Eliminado!", "El Registro se elimino correctamente.", "success");
				descargarFacturacion($("#personaid").val());
			},
			error: function ( error )
			{
				console.log(error);
				swal("Opps!", error.responseJSON.message, "error");
			}
		});
	});

	if ( e != null )
		e.preventDefault();
}

/** ENFERMEDADES */
function llenarEnfermedades( enfermedades ) {
	$("#tabla-enfermedades").dataTable().fnClearTable();

	var xIndex = 0;
	$.each( enfermedades, function(index, value){
		var acciones;
		acciones = "<div class='btn-tollbar' role='toolbar'>" +
					"<div class='btn-group' role='group'>" +
						"<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-eliminar-enfermedad'><i class='fa fa-trash-o'></i> Eliminar</a> " +
					"</div>" +
				"</div>";

		$("#tabla-enfermedades").dataTable().fnAddData([
			++xIndex,
			value.tipoenfermedad.nombre,
			acciones
		]);
	});
}

function descargarEnfermedades( persona ) {
	$.ajax({
		type: 		"GET",
		url: 		"api/paciente/"+persona+"/enfermedades",
		dataType: 	"json",
		data: 		{},
		success: function ( enfermedades )
		{
			llenarEnfermedades(enfermedades);
		},
		error: function ( error )
		{
			console.log(error);
		}
	});
}

function crearEnfermedad( e ) {
	$.ajax({
		type: 		"POST",
		url: 		"api/pacienteenfermedad",
		dataType: 	"json",
		data: 		$(this).serialize()+"&paciente="+$("#pacienteid").val(),
		success: function( respuesta )
		{
			descargarEnfermedades($("#pacienteid").val());
			$("#form-enfermedades")[0].reset();
			swal("Buen Trabajo!", "Enfermedad Agregada Correctamente", "success");
		},
		error: function( error )
		{
			console.log(error);
			swal("Opps!", error.responseJSON.message, "error");
		}
	});

    if ( e != null )
		e.preventDefault();
        e.stopPropagation();
}

function eliminarEnfermedad( e ) {
	var idreg = $(e.target).closest("a").attr("idreg");
	swal({
		title: "Estas Seguro?",
		text: "El registro no se podra recuperar!",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Si, elimarlo!",
		cancelButtonText: "Cancelar",
		closeOnConfirm: false
	},
	function(){
		$.ajax({
			type: 		"DELETE",
			url: 		"api/pacienteenfermedad/"+idreg,
			dataType: 	"json",
			data: 		{},
			success: function ( respuesta )
			{
				swal("Eliminado!", "El Registro se elimino correctamente.", "success");
				descargarEnfermedades($("#pacienteid").val());
			},
			error: function ( error )
			{
				swal("Opps!", error.responseJSON.message, "error");
			}
		});
	});

	if ( e != null )
		e.preventDefault();
}

function bloquearTabs() {
    $(".lidir").addClass("disabled");
    $(".litel").addClass("disabled");
    $(".licor").addClass("disabled");
    $(".lifac").addClass("disabled");
	$(".lidpa").addClass("disabled");
	$(".lihme").addClass("disabled");
	$(".lihod").addClass("disabled");
	$(".lienf").addClass("disabled");

    $(".tabdir").removeAttr("data-toggle");
    $(".tabtel").removeAttr("data-toggle");
    $(".tabcor").removeAttr("data-toggle");
    $(".tabfac").removeAttr("data-toggle");
	$(".tabdpa").removeAttr("data-toggle");
	$(".tabhme").removeAttr("data-toggle");
	$(".tabhod").removeAttr("data-toggle");
	$(".tabenf").removeAttr("data-toggle");

    $(".tabdir").removeAttr("href");
    $(".tabtel").removeAttr("href");
    $(".tabcor").removeAttr("href");
    $(".tabfac").removeAttr("href");
	$(".tabdpa").removeAttr("href");
	$(".tabhme").removeAttr("href");
	$(".tabhod").removeAttr("href");
	$(".tabenf").removeAttr("href");
}

function habilitarTabs() {
    $(".lidir").removeClass("disabled");
    $(".litel").removeClass("disabled");
    $(".licor").removeClass("disabled");
    $(".lifac").removeClass("disabled");
	$(".lidpa").removeClass("disabled");
	$(".lihme").removeClass("disabled");
	$(".lihod").removeClass("disabled");
	$(".lienf").removeClass("disabled");

    $(".tabdir").attr("data-toggle", "tab");
    $(".tabtel").attr("data-toggle", "tab");
    $(".tabcor").attr("data-toggle", "tab");
    $(".tabfac").attr("data-toggle", "tab");
	$(".tabdpa").attr("data-toggle", "tab");
	$(".tabhme").attr("data-toggle", "tab");
	$(".tabhod").attr("data-toggle", "tab");
	$(".tabenf").attr("data-toggle", "tab");

    $(".tabdir").attr("href", "#direcciones");
    $(".tabtel").attr("href", "#telefonos");
    $(".tabcor").attr("href", "#correos");
    $(".tabfac").attr("href", "#facturacion");
	$(".tabdpa").attr("href", "#datospaciente");
	$(".tabhme").attr("href", "#historiamedica");
	$(".tabhod").attr("href", "#historiaodontologica");
	$(".tabenf").attr("href", "#enfermedades");
}

function llenarTabla( e ) 
{
	$.ajax({
		type:'GET',
		url:'api/paciente',
		dataType:'json',
		data:{},
		success:function( registros )
		{
			
			$("#tabla-registros").dataTable().fnClearTable();

			var xIndex = 0;
			$.each( registros, function(index, value){
				var acciones;
                acciones = "<div class='btn-tollbar' role='toolbar'>" +
                            "<div class='btn-group' role='group'>" +
                                "<a href='#' idreg='"+value.id+"' class='btn btn-default btn-sm btn-editar'><i class='fa fa-pencil-square-o'></i> Editar</a> " +
                            "</div>" +
                        "</div>";
            
				
				$("#tabla-registros").dataTable().fnAddData([
					++xIndex,
					value.persona.nombres,
                    value.persona.apellidos,
					acciones
				]);
			});
			
		},
		error: function( error )
		{
			console.log(error);
		}
	});
	

	if ( e != null )
		e.preventDefault();
}


function llenarProfesiones( e ) {
    $.ajax({
		type:'GET',
		url:'api/tipoprofesion',
		dataType:'json',
		data:{},
		success:function( registros )
		{
			var optionTipoProfesion = $("#form-datosgenerales #profesion");
            optionTipoProfesion.find('option').remove();
            $.each(registros, function(index, value){
                optionTipoProfesion.append($("<option />").val(value.id).text(value.nombre));
            });
		},
		error: function( error )
		{
			console.log(error);
		}
	});
	

	if ( e != null )
		e.preventDefault();
}

function llenarTipoDireccion( e ) {
    $.ajax({
		type:'GET',
		url:'api/tipodireccion',
		dataType:'json',
		data:{},
		success:function( registros )
		{
			var optionTipoProfesion = $("#form-direcciones #tipodireccion");
            optionTipoProfesion.find('option').remove();
            $.each(registros, function(index, value){
                optionTipoProfesion.append($("<option />").val(value.id).text(value.nombre));
            });
		},
		error: function( error )
		{
			console.log(error);
		}
	});
	

	if ( e != null )
		e.preventDefault();
}

function llenarTipoTelefono( e ) {
    $.ajax({
		type:'GET',
		url:'api/tipotelefono',
		dataType:'json',
		data:{},
		success:function( registros )
		{
			var optionTipoProfesion = $("#form-telefonos #tipotelefono");
            optionTipoProfesion.find('option').remove();
            $.each(registros, function(index, value){
                optionTipoProfesion.append($("<option />").val(value.id).text(value.nombre));
            });
		},
		error: function( error )
		{
			console.log(error);
		}
	});
	

	if ( e != null )
		e.preventDefault();
}

function llenarTipoCorreo( e ) {
    $.ajax({
		type:'GET',
		url:'api/tipocorreo',
		dataType:'json',
		data:{},
		success:function( registros )
		{
			var optionTipoProfesion = $("#form-correos #tipocorreo");
            optionTipoProfesion.find('option').remove();
            $.each(registros, function(index, value){
                optionTipoProfesion.append($("<option />").val(value.id).text(value.nombre));
            });
		},
		error: function( error )
		{
			console.log(error);
		}
	});
	

	if ( e != null )
		e.preventDefault();
}

function llenarPais( e ) {
	$.ajax({
		type:'GET',
		url:'api/paises',
		dataType:'json',
		data:{},
		success:function( registros )
		{
			var optionTipoProfesion = $("#form-direcciones #pais");
            optionTipoProfesion.find('option').remove();
            $.each(registros, function(index, value){
                optionTipoProfesion.append($("<option />").val(value.id).text(value.nombre));
            });
			llenarDepartamentos();
		},
		error: function( error )
		{
			console.log(error);
		}
	});

	if ( e != null )
		e.preventDefault();
}

function llenarDepartamentos( e ) {
	$.ajax({
		type:'GET',
		url:'api/paises/'+$("#form-direcciones #pais").val()+"/departamentos",
		dataType:'json',
		data:{},
		success:function( registros )
		{
			var optionTipoProfesion = $("#form-direcciones #departamento");
            optionTipoProfesion.find('option').remove();
			if (registros.length > 0) {
				$.each(registros, function(index, value){
					optionTipoProfesion.append($("<option />").val(value.id).text(value.nombre));
				});
			} else {
				optionTipoProfesion.append($("<option />").val(0).text("Sin Seleccionar"));
			}
			llenarMunicipios();
		},
		error: function( error )
		{
			console.log(error);
		}
	});
	
	if ( e != null )
		e.preventDefault();
}

function llenarMunicipios( e ) {
	$.ajax({
		type:'GET',
		url:'api/departamentos/'+$("#form-direcciones #departamento").val()+"/municipios",
		dataType:'json',
		data:{},
		success:function( registros )
		{
			var optionTipoProfesion = $("#form-direcciones #municipio");
            optionTipoProfesion.find('option').remove();
            if (registros.length > 0) {
				$.each(registros, function(index, value){
					optionTipoProfesion.append($("<option />").val(value.id).text(value.nombre));
				});
			} else {
				optionTipoProfesion.append($("<option />").val(0).text("Sin Seleccionar"));
			}
		},
		error: function( error )
		{
			console.log(error);
		}
	});
	
	if ( e != null )
		e.preventDefault();
}

function llenarMotivoConsulta( e ) {
    $.ajax({
		type:'GET',
		url:'api/motivoconsulta',
		dataType:'json',
		data:{},
		success:function( registros )
		{
			var optionTipoProfesion = $("#form-datospaciente #motivoconsulta");
            optionTipoProfesion.find('option').remove();
            $.each(registros, function(index, value){
                optionTipoProfesion.append($("<option />").val(value.id).text(value.nombre));
            });
		},
		error: function( error )
		{
			console.log(error);
		}
	});
	
	if ( e != null )
		e.preventDefault();
}

function llenarCentroEmergencia( e ) {
    $.ajax({
		type:'GET',
		url:'api/centroemergencia',
		dataType:'json',
		data:{},
		success:function( registros )
		{
			var optionTipoProfesion = $("#form-datospaciente #centroemergencia");
            optionTipoProfesion.find('option').remove();
            $.each(registros, function(index, value){
                optionTipoProfesion.append($("<option />").val(value.id).text(value.nombre));
            });
		},
		error: function( error )
		{
			console.log(error);
		}
	});
	
	if ( e != null )
		e.preventDefault();
}

function llenarTipoPaciente( e ) {
    $.ajax({
		type:'GET',
		url:'api/tipopaciente',
		dataType:'json',
		data:{},
		success:function( registros )
		{
			var optionTipoProfesion = $("#form-datospaciente #tipopaciente");
            optionTipoProfesion.find('option').remove();
            $.each(registros, function(index, value){
                optionTipoProfesion.append($("<option />").val(value.id).text(value.nombre));
            });
		},
		error: function( error )
		{
			console.log(error);
		}
	});
	
	if ( e != null )
		e.preventDefault();
}

function llenarTipoSangre( e ) {
    $.ajax({
		type:'GET',
		url:'api/tiposangre',
		dataType:'json',
		data:{},
		success:function( registros )
		{
			var optionTipoProfesion = $("#form-datospaciente #tiposangre");
            optionTipoProfesion.find('option').remove();
            $.each(registros, function(index, value){
                optionTipoProfesion.append($("<option />").val(value.id).text(value.nombre));
            });
		},
		error: function( error )
		{
			console.log(error);
		}
	});
	
	if ( e != null )
		e.preventDefault();
}

function llenarMedioPublicitario( e ) {
    $.ajax({
		type:'GET',
		url:'api/mediopublicitario',
		dataType:'json',
		data:{},
		success:function( registros )
		{
			var optionTipoProfesion = $("#form-datospaciente #mediopublicitario");
            optionTipoProfesion.find('option').remove();
            $.each(registros, function(index, value){
                optionTipoProfesion.append($("<option />").val(value.id).text(value.nombre));
            });
		},
		error: function( error )
		{
			console.log(error);
		}
	});
	
	if ( e != null )
		e.preventDefault();
}

function llenarTipoEnfermedad( e ) {
	$.ajax({
		type:'GET',
		url:'api/tipoenfermedad',
		dataType:'json',
		data:{},
		success:function( registros )
		{
			var optionTipoProfesion = $("#form-enfermedades #tipoenfermedad");
            optionTipoProfesion.find('option').remove();
            $.each(registros, function(index, value){
                optionTipoProfesion.append($("<option />").val(value.id).text(value.nombre));
            });
		},
		error: function( error )
		{
			console.log(error);
		}
	});

	if ( e != null )
		e.preventDefault();
}

function closeBlockPaciente( e ) {
	$("#block-paciente").addClass("hide");
	if ( e != null )
		e.preventDefault();
}
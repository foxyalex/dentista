<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonaCorreo extends Model
{
    protected $table = 'PersonaCorreo';

    public function persona() {
        return $this->hasOne('App\Persona', 'id', 'persona');
    }

    public function tipocorreo() {
        return $this->hasOne('App\TipoCorreo', 'id', 'tipocorreo');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonaDireccion extends Model
{
    protected $table = 'PersonaDireccion';

    public function pais() {
        return $this->hasOne('App\Paises', 'id', 'pais');
    }

    public function departamento() {
        return $this->hasOne('App\Departamentos', 'id', 'departamento');
    }

    public function municipio() {
        return $this->hasOne('App\Municipios', 'id', 'municipio');
    }

    public function persona() {
        return $this->hasOne('App\Persona', 'id', 'persona');
    }

    public function tipodireccion() {
        return $this->hasOne('App\TipoDireccion', 'id', 'tipodireccion');
    }
}

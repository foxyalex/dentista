<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    protected $table = 'Paciente';

    public function persona() {
        return $this->hasOne('App\Persona', 'id', 'persona');
    }

    public function gmotivoconsulta() {
        return $this->hasOne('App\MotivoConsulta', 'id', 'motivoconsulta');
    }

    public function gcentroemergencia() {
        return $this->hasOne('App\CentroEmergencia', 'id', 'centroemergencia');
    }

    public function gtipopaciente() {
        return $this->hasOne('App\TipoPaciente', 'id', 'tipopaciente');
    }

    public function gtiposangre() {
        return $this->hasOne('App\TipoSangre', 'id', 'tiposangre');
    }

    public function gmediopublicitario() {
        return $this->hasOne('App\MedioPublicitario', 'id', 'mediopublicitario');
    }

    public function enfermedades() {
        return $this->hasMany('App\PacienteEnfermedad', 'paciente')->with('tipoenfermedad');
    }

    public function personaCompleta() {
        return $this->hasOne('App\Persona', 'id', 'persona')->with('telefonos', 'correos', 'direcciones', 'facturaciones');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CentroEmergenciaTelefono extends Model
{
    protected $table = 'CentroEmergenciaTelefono';

     public function centroemergencia() {
        return $this->hasOne('App\CentroEmergencia', 'id', 'centroemergencia');
    }

    public function tipotelefono() {
        return $this->hasOne('App\TipoTelefono', 'id', 'tipotelefono');
    }
}

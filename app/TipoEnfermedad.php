<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoEnfermedad extends Model
{
    protected $table = 'TipoEnfermedad';
}

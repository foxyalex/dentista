<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SucursalesCorreo extends Model
{
    protected $table = 'SucursalesCorreo';

    public function sucursal() {
        return $this->hasOne('App\Sucursales', 'id', 'sucursal');
    }

    public function tipocorreo() {
        return $this->hasOne('App\TipoCorreo', 'id', 'tipocorreo');
    }
}

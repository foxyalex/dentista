<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table = 'Usuario';

    public function persona() {
        return $this->hasOne('App\Persona', 'id', 'persona');
    }

    public function personaCompleta() {
        return $this->hasOne('App\Persona', 'id', 'persona')->with('telefonos', 'correos', 'direcciones', 'facturaciones');
    }
}

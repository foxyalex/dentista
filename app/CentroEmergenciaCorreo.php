<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CentroEmergenciaCorreo extends Model
{
    protected $table = 'CentroEmergenciaCorreo';

    public function centroemergencia() {
        return $this->hasOne('App\CentroEmergencia', 'id', 'centroemergencia');
    }

    public function tipocorreo() {
        return $this->hasOne('App\TipoCorreo', 'id', 'tipocorreo');
    }
}

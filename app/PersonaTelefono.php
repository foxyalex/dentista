<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonaTelefono extends Model
{
    protected $table = 'PersonaTelefono';

    public function persona() {
        return $this->hasOne('App\Persona', 'id', 'persona');
    }

    public function tipotelefono() {
        return $this->hasOne('App\TipoTelefono', 'id', 'tipotelefono');
    }
}

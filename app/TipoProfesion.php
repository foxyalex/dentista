<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoProfesion extends Model
{
    protected $table = 'TipoProfesion';
}

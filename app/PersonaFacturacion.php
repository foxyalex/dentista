<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonaFacturacion extends Model
{
    protected $table = 'PersonaFacturacion';

    public function persona() {
        return $this->hasOne('App\Persona', 'id', 'persona');
    }
}

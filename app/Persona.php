<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $table = 'Persona';

    public function telefonos() {
        return $this->hasMany('App\PersonaTelefono', 'persona')->with('tipotelefono');
    }

    public function correos() {
        return $this->hasMany('App\PersonaCorreo', 'persona')->with('tipocorreo');
    }

    public function direcciones() {
        return $this->hasMany('App\PersonaDireccion', 'persona')->with('tipodireccion', 'pais', 'departamento', 'municipio');
    }

    public function facturaciones() {
        return $this->hasMany('App\PersonaFacturacion', 'persona');
    }
}
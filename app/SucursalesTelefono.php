<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SucursalesTelefono extends Model
{
    protected $table = 'SucursalesTelefono';

    public function persona() {
        return $this->hasOne('App\Sucursales', 'id', 'sucursal');
    }

    public function tipotelefono() {
        return $this->hasOne('App\TipoTelefono', 'id', 'tipotelefono');
    }
}

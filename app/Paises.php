<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paises extends Model
{
    protected $table = 'Paises';

    public function departamentos() {
        return $this->hasMany('App\Departamentos', 'pais');
    }
}

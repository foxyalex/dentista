<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CentroEmergencia extends Model
{
    protected $table = 'CentroEmergencia';

    public function telefonos() {
        return $this->hasMany('App\CentroEmergenciaTelefono', 'centroemergencia')->with('tipotelefono');
    }

    public function correos() {
        return $this->hasMany('App\CentroEmergenciaCorreo', 'centroemergencia')->with('tipocorreo');
    }
}

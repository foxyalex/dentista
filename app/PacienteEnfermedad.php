<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PacienteEnfermedad extends Model
{
    protected $table = 'PacienteEnfermedad';

    public function paciente() {
        return $this->hasOne('App\Paciente', 'id', 'paciente');
    }

    public function tipoenfermedad() {
        return $this->hasOne('App\TipoEnfermedad', 'id', 'tipoenfermedad');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoCorreo extends Model
{
    protected $table = 'TipoCorreo';
}

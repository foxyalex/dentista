<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use Validator;
use App\PersonaFacturacion;

class PersonaFacturacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(PersonaFacturacion::all(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre'         => 'required',
            'direccion'      => 'required',
            'nit'      => 'required',
            'preferida'      => 'required',
            'persona'      => 'required'
        ]);

        if ($validator->fails()) {
            $returnData = array(
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator->messages()->toJson()
            );
            return Response::json($returnData, 400);
        } else {
            try {
                $newObject = new PersonaFacturacion();
                $newObject->nombre = $request->get('nombre');
                $newObject->direccion = $request->get('direccion');
                $newObject->nit = $request->get('nit');
                $newObject->preferida = $request->get('preferida');
                $newObject->persona = $request->get('persona');
                $newObject->save();
                return Response::json($newObject, 200);
            }
            catch(Exception $e) {
                $returnData = array(
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = PersonaFacturacion::find($id);
        if ($objectSee) {
            return Response::json($objectSee, 200);
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'Not found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = PersonaFacturacion::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->nombre = $request->get('nombre', $objectUpdate->nombre);
                $objectUpdate->nit = $request->get('nit', $objectUpdate->nit);
                $objectUpdate->direccion = $request->get('direccion', $objectUpdate->direccion);
                $objectUpdate->preferida = $request->get('preferida', $objectUpdate->preferida);
                $objectUpdate->persona = $request->get('persona', $objectUpdate->persona);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            }
            catch (Exception $e) {
                $returnData = array(
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'Not found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = PersonaFacturacion::find($id);
        if ($objectDelete) {
            try {
                PersonaFacturacion::destroy($id);
                return Response::json($objectDelete, 200);
            }
            catch (Exception $e) {
                $returnData = array(
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'Not found'
            );
            return Response::json($returnData, 404);
        }
    }
}

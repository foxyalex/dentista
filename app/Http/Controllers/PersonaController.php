<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use Validator;
use App\Persona;
use App\PersonaTelefono;
use App\PersonaCorreo;
use App\PersonaDireccion;
use App\PersonaFacturacion;

class PersonaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(Persona::all(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombres'          => 'required',
            'apellidos'        => 'required',
            'nacimiento'       => 'required',
            'dni'              => 'required',
            'estadocivil'      => 'required',
            'genero'           => 'required',
            'profesion'        => 'required',
        ]);

        if ($validator->fails()) {
            $returnData = array(
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator->messages()->toJson()
            );
            return Response::json($returnData, 400);
        } else {
            try {
                $newObject = new Persona();
                $newObject->nombres = $request->get('nombres');
                $newObject->apellidos = $request->get('apellidos');
                $newObject->casada = $request->get('casada', '');
                $newObject->nacimiento = date("Y/m/d", strtotime(str_replace("%2F", "/", $request->get('nacimiento'))));
                $newObject->dni = $request->get('dni');
                $newObject->genero = $request->get('genero');
                $newObject->estadocivil = $request->get('estadocivil');
                $newObject->profesion = $request->get('profesion');
                $newObject->save();
                return Response::json($newObject, 200);
            }
            catch(Exception $e) {
                $returnData = array(
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = Persona::find($id);
        if ($objectSee) {
            $objectSee->telefonos;
            return Response::json($objectSee, 200);
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'Not found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = Persona::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->nombres = $request->get('nombres', $objectUpdate->nombres);
                $objectUpdate->apellidos = $request->get('apellidos', $objectUpdate->apellidos);
                $objectUpdate->casada = $request->get('casada', $objectUpdate->casada);
                $objectUpdate->nacimiento = date("Y/m/d", strtotime(str_replace("%2F", "/", $request->get('nacimiento'))));
                $objectUpdate->dni = $request->get('dni', $objectUpdate->dni);
                $objectUpdate->genero = $request->get('genero', $objectUpdate->genero);
                $objectUpdate->estadocivil = $request->get('estadocivil', $objectUpdate->estadocivil);
                $objectUpdate->profesion = $request->get('profesion', $objectUpdate->profesion);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            }
            catch (Exception $e) {
                $returnData = array(
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'Not found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = Persona::find($id);
        if ($objectDelete) {
            try {
                Persona::destroy($id);
                return Response::json($objectDelete, 200);
            }
            catch (Exception $e) {
                $returnData = array(
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'Not found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function telefonos($id) {
        $objectSee = PersonaTelefono::whereRaw('persona = ?', [$id])->with('tipotelefono')->get();
        if ($objectSee) {
            return Response::json($objectSee, 200);
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'Not found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function correos($id) {
        $objectSee = PersonaCorreo::whereRaw('persona = ?', [$id])->with('tipocorreo')->get();
        if ($objectSee) {
            return Response::json($objectSee, 200);
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'Not found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function direcciones($id) {
        $objectSee = PersonaDireccion::whereRaw('persona = ?', [$id])->with('tipodireccion', 'pais', 'departamento', 'municipio')->get();
        if ($objectSee) {
            return Response::json($objectSee, 200);
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'Not found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function facturacion($id) {
        $objectSee = PersonaFacturacion::whereRaw('persona = ?', [$id])->get();
        if ($objectSee) {
            return Response::json($objectSee, 200);
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'Not found'
            );
            return Response::json($returnData, 404);
        }
    }
}

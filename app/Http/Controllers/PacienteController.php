<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use Validator;
use App\Paciente;
use App\PacienteEnfermedad;
class PacienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(Paciente::with('persona')->get(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'persona'          => 'required'
        ]);

        if ($validator->fails()) {
            $returnData = array(
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator->messages()->toJson()
            );
            return Response::json($returnData, 400);
        } else {
            try {
                $newObject = new Paciente();
                $newObject->persona = $request->get('persona');
                $newObject->save();
                return Response::json($newObject, 200);
            }
            catch(Exception $e) {
                $returnData = array(
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = Paciente::find($id);
        if ($objectSee) {
            $objectSee->personaCompleta;
            $objectSee->enfermedades;
            $objectSee->gmotivoconsulta;
            $objectSee->gcentroemergencia;
            $objectSee->gtipopaciente;
            $objectSee->gtiposangre;
            $objectSee->gmediopublicitario;
            return Response::json($objectSee, 200);
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'Not found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = Paciente::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->persona = $request->get('persona', $objectUpdate->persona);
                $objectUpdate->motivoconsulta = $request->get('motivoconsulta', $objectUpdate->motivoconsulta);
                $objectUpdate->centroemergencia = $request->get('centroemergencia', $objectUpdate->centroemergencia);
                $objectUpdate->aspectosrelevantes = $request->get('aspectosrelevantes', $objectUpdate->aspectosrelevantes);
                $objectUpdate->tipopaciente = $request->get('tipopaciente', $objectUpdate->tipopaciente);
                $objectUpdate->tiposangre = $request->get('tiposangre', $objectUpdate->tiposangre);
                $objectUpdate->polizaseguro = $request->get('polizaseguro', $objectUpdate->polizaseguro);
                $objectUpdate->mediopublicitario = $request->get('mediopublicitario', $objectUpdate->mediopublicitario);
                $objectUpdate->montoautorizado = $request->get('montoautorizado', $objectUpdate->montoautorizado);
                $objectUpdate->hmproblemas = $request->get('hmproblemas', $objectUpdate->hmproblemas);
                $objectUpdate->hmcirugias = $request->get('hmcirugias', $objectUpdate->hmcirugias);
                $objectUpdate->hmtrastornocorazon = $request->get('hmtrastornocorazon', $objectUpdate->hmtrastornocorazon);
                $objectUpdate->hmtrastornocorazoncomentario = $request->get('hmtrastornocorazoncomentario', $objectUpdate->hmtrastornocorazoncomentario);
                $objectUpdate->hmmedicamentos = $request->get('hmmedicamentos', $objectUpdate->hmmedicamentos);
                $objectUpdate->hmfuma = $request->get('hmfuma', $objectUpdate->hmfuma);
                $objectUpdate->hmbebidas = $request->get('hmbebidas', $objectUpdate->hmbebidas);
                $objectUpdate->hmtratamiento = $request->get('hmtratamiento', $objectUpdate->hmtratamiento);
                $objectUpdate->hmalergico = $request->get('hmalergico', $objectUpdate->hmalergico);
                $objectUpdate->hmaccidentes = $request->get('hmaccidentes', $objectUpdate->hmaccidentes);
                $objectUpdate->hmcomentarios = $request->get('hmcomentarios', $objectUpdate->hmcomentarios);
                $objectUpdate->houltimavisita = $request->get('houltimavisita', $objectUpdate->houltimavisita);
                $objectUpdate->hotratamientodental = $request->get('hotratamientodental', $objectUpdate->hotratamientodental);
                $objectUpdate->hocomentariosrelevantes = $request->get('hocomentariosrelevantes', $objectUpdate->hocomentariosrelevantes);
                $objectUpdate->homolestiaactual = $request->get('homolestiaactual', $objectUpdate->homolestiaactual);
                $objectUpdate->hoaccidentes = $request->get('hoaccidentes', $objectUpdate->hoaccidentes);
                $objectUpdate->hoextraccion = $request->get('hoextraccion', $objectUpdate->hoextraccion);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            }
            catch (Exception $e) {
                $returnData = array(
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'Not found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = Paciente::find($id);
        if ($objectDelete) {
            try {
                Paciente::destroy($id);
                return Response::json($objectDelete, 200);
            }
            catch (Exception $e) {
                $returnData = array(
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'Not found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function enfermedades($id) {
        $objectSee = PacienteEnfermedad::whereRaw('paciente = ?', [$id])->with('tipoenfermedad')->get();
        if ($objectSee) {
            return Response::json($objectSee, 200);
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'Not found'
            );
            return Response::json($returnData, 404);
        }
    }
}

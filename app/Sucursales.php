<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sucursales extends Model
{
    protected $table = 'Sucursales';

    public function telefonos() {
        return $this->hasMany('App\SucursalesTelefono', 'sucursal')->with('tipotelefono');
    }

    public function correos() {
        return $this->hasMany('App\SucursalesCorreo', 'sucursal')->with('tipocorreo');
    }
}

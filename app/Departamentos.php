<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departamentos extends Model
{
    protected $table = 'Departamentos';

    public function municipios() {
        return $this->hasMany('App\Municipios', 'departamento');
    }
}

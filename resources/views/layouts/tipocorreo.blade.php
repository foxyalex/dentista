<div class="row bg-title">
    <div class="col-lg-12">
        <h4 class="page-title">Tipos de Correos</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <h3>Lista</h3>
            <p class="m-t-20">
                <button type="button" class="btn btn-info btn-crear">Crear Nuevo</button>            
            </p>
            <table id="tabla-registros" class="table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Acciones</th>                        
                    </tr>
                </thead>    
            </table>
        </div>
    </div>
</div>

<!-- modal crear -->
<div id="modal-crear" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="createTitleModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="createTitleModal">Crear Registro</h4>
            </div>
            <form id="form-crear">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Nombre:</label>
                        <input type="text" class="form-control" name="nombre">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Guardar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- modal crear -->
<div id="modal-editar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="createTitleModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="createTitleModal">Editar Registro</h4>
            </div>
            <form id="form-editar">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Nombre:</label>
                        <input type="text" class="form-control" name="nombre" id="nombre">
                        <input type="hidden" id="updateid">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Guardar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script type="text/javascript" src="js/tipocorreo.js"></script>
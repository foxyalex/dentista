<div class="row bg-title">
    <div class="col-lg-12">
        <h4 class="page-title">Tipos de Contactos</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <h3>Lista</h3>
            <p class="m-t-20">
                <button type="button" class="btn btn-info btn-crear">Crear Nuevo</button>            
            </p>
            <table id="tabla-registros" class="table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Pais</th>
                        <th>Acciones</th>                        
                    </tr>
                </thead>    
            </table>
        </div>
    </div>
</div>

<div class="row hide" id="block-departamentos">
    <div class="col-sm-12">
        <div class="panel panel-info">
            <div class="panel-heading"> Departamentos
                <div class="pull-right">
                    <a href="#" class="close-departamentos"><i class="ti-close"></i></a> 
                </div>
            </div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                    <input type="hidden" id="departamento_actual">
                    <h3 id="title-departamento">Departamentos</h3>
                    <p class="m-t-20">
                        <button type="button" class="btn btn-info btn-crear-departamento">Crear Departamento</button>            
                    </p>
                    <table id="tabla-departamentos" class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Departamento</th>
                                <th>Acciones</th>                        
                            </tr>
                        </thead>    
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row hide" id="block-municipios">
    <div class="col-sm-12">
        <div class="panel panel-info">
            <div class="panel-heading"> Municipios
                <div class="pull-right">
                    <a href="#" class="close-municipios"><i class="ti-close"></i></a> 
                </div>
            </div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                    <input type="hidden" id="municipio_actual">
                    <h3 id="title-municipio">Municipio</h3>
                    <p class="m-t-20">
                        <button type="button" class="btn btn-info btn-crear-municipio">Crear Municipio</button>            
                    </p>
                    <table id="tabla-municipios" class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Municipio</th>
                                <th>Acciones</th>                        
                            </tr>
                        </thead>    
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal crear -->
<div id="modal-crear" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="createTitleModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="createTitleModal">Crear Registro</h4>
            </div>
            <form id="form-crear">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Nombre:</label>
                        <input type="text" class="form-control" name="nombre">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Guardar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- modal editar -->
<div id="modal-editar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="createTitleModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="createTitleModal">Editar Registro</h4>
            </div>
            <form id="form-editar">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Nombre:</label>
                        <input type="text" class="form-control" name="nombre" id="nombre">
                        <input type="hidden" id="updateid">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Guardar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- modal eliminar -->
<div id="modal-eliminar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="createTitleModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title text-white" id="createTitleModal">Eliminar Registro</h4>
            </div>
            <form id="form-eliminar">
                <div class="modal-body">
                    <div class="form-group">
                        <h3>Estas seguro de eliminar el Registro?</h3>
                        <input type="hidden" id="updateid">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-danger waves-effect waves-light">Eliminar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- modal crear departamento-->
<div id="modal-crear-departamento" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="createTitleModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="createTitleModal">Crear Departamento</h4>
            </div>
            <form id="form-crear-departamento">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Nombre:</label>
                        <input type="text" class="form-control" name="nombre">
                        <input type="hidden" name="pais" id="paisId">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Guardar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- modal editar -->
<div id="modal-editar-departamento" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="createTitleModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="createTitleModal">Editar Departamento</h4>
            </div>
            <form id="form-editar-departamento">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Nombre:</label>
                        <input type="text" class="form-control" name="nombre" id="nombre">
                        <input type="hidden" name="pais" id="paisId">
                        <input type="hidden" id="updateid">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Guardar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- modal eliminar departamento -->
<div id="modal-eliminar-departamento" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="createTitleModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title text-white" id="createTitleModal">Eliminar Departamento</h4>
            </div>
            <form id="form-eliminar-departamento">
                <div class="modal-body">
                    <div class="form-group">
                        <h3>Estas seguro de eliminar el Registro?</h3>
                        <input type="hidden" id="updateid">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-danger waves-effect waves-light">Eliminar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- modal crear municipio-->
<div id="modal-crear-municipio" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="createTitleModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="createTitleModal">Crear Municipio</h4>
            </div>
            <form id="form-crear-municipio">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Nombre:</label>
                        <input type="text" class="form-control" name="nombre">
                        <input type="hidden" name="departamento" id="departamentoId">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Guardar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- modal editar municipio-->
<div id="modal-editar-municipio" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="createTitleModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="createTitleModal">Editar Municipio</h4>
            </div>
            <form id="form-editar-municipio">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Nombre:</label>
                        <input type="text" class="form-control" name="nombre" id="nombre">
                        <input type="hidden" name="departamento" id="departamentoId">
                        <input type="hidden" id="updateid">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Guardar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- modal eliminar municipio-->
<div id="modal-eliminar-municipio" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="createTitleModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title text-white" id="createTitleModal">Eliminar Municipio</h4>
            </div>
            <form id="form-eliminar-municipio">
                <div class="modal-body">
                    <div class="form-group">
                        <h3>Estas seguro de eliminar el Registro?</h3>
                        <input type="hidden" id="updateid">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-danger waves-effect waves-light">Eliminar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script type="text/javascript" src="js/paises.js"></script>
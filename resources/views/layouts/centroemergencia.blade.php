<div class="row bg-title">
    <div class="col-lg-12">
        <h4 class="page-title">Centro Emergencia</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <h3>Lista</h3>
            <p class="m-t-20">
                <button type="button" class="btn btn-info btn-crear">Crear Nuevo</button>            
            </p>
            <table id="tabla-registros" class="table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Estado</th>
                        <th>Acciones</th>                        
                    </tr>
                </thead>    
            </table>
        </div>
    </div>
</div>

<div class="row hide" id="block-centroemergencia">
    <div class="col-sm-12">
        <div class="panel panel-info">
            <div class="panel-heading"> Datos Centro Emergencia
                <div class="pull-right">
                    <a href="#" class="close-centroemergencia"><i class="ti-close"></i></a>
                    <input type="hidden" id="centroemergenciaid">
                </div>
            </div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <div class="white-box">
                                <!-- Nav tabs -->
                                <ul class="nav customtab nav-tabs" role="tablist">
                                    <li role="presentation" class="lidg active"><a href="#datosgenerales" aria-controls="home" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Datos Generales</span></a></li>
                                    <li role="presentation" class="litel"><a class="tabtel" href="#telefonos" aria-controls="messages" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-email"></i></span> <span class="hidden-xs">Telefonos</span></a></li>
                                    <li role="presentation" class="licor"><a class="tabcor" href="#correos" aria-controls="settings" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-settings"></i></span> <span class="hidden-xs">Correos</span></a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <!-- Datos Generales -->
                                    <div role="tabpanel" class="tab-pane fade in active" id="datosgenerales">
                                        <form id="form-datosgenerales">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Nombre:</label>
                                                            <input type="text" class="form-control" name="nombre" id="nombre">
                                                        </div>
                                                        <p class="m-t-20">
                                                            <button type="submit" class="btn btn-info" id="btn-fdg">Crear Nuevo</button>            
                                                        </p>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Estado:</label>
                                                            <select id="estado" name="estado" class="form-control">
                                                                <option value="1">Activo</option>
                                                                <option value="0">No Activo</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="clearfix"></div>
                                    </div>
                                    <!-- Telefonos -->
                                    <div role="tabpanel" class="tab-pane fade" id="telefonos">
                                        <div class="row">
                                            <form id="form-telefonos">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Numero:</label>
                                                                <input type="text" class="form-control" name="numero" id="numero">
                                                                <input type="hidden" id="updateid">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Tipo Telefono:</label>
                                                                <select class="form-control" id="tipotelefono" name="tipotelefono">
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Preferida:</label>
                                                                <select class="form-control" id="preferida" name="preferida">
                                                                    <option value="0">No</option>
                                                                    <option value="1">Si</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                     <p class="m-t-20">
                                                        <button type="submit" class="btn btn-info btn-ftel">Agregar Teléfono</button>
                                                        <button type="button" class="btn btn-danger btn-ctel hide">Cancelar</button>          
                                                    </p>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="white-box">
                                                    <table id="tabla-telefonos" class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Teléfono</th>
                                                                <th>Tipo Teléfono</th>
                                                                <th>Preferida</th>
                                                                <th>Acciones</th>                        
                                                            </tr>
                                                        </thead>    
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <!-- Correos -->
                                    <div role="tabpanel" class="tab-pane fade" id="correos">
                                        <div class="row">
                                            <form id="form-correos">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Correo:</label>
                                                                <input type="text" class="form-control" name="correo" id="correo">
                                                                <input type="hidden" id="updateid">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Tipo Correo:</label>
                                                                <select class="form-control" id="tipocorreo" name="tipocorreo">
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Preferida:</label>
                                                                <select class="form-control" id="preferida" name="preferida">
                                                                    <option value="0">No</option>
                                                                    <option value="1">Si</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <p class="m-t-20">
                                                        <button type="submit" class="btn btn-info btn-fcor">Agregar Correo</button>
                                                        <button type="button" class="btn btn-danger btn-ccor hide">Cancelar</button>          
                                                    </p>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="white-box">
                                                    <table id="tabla-correos" class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Correo</th>
                                                                <th>Tipo Correo</th>
                                                                <th>Preferida</th>
                                                                <th>Acciones</th>                        
                                                            </tr>
                                                        </thead>    
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" src="js/centroemergencia.js"></script>
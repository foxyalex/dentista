<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <title>Grupo Dent</title>
    <!-- Bootstrap Core CSS -->
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="bower_components/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="bower_components/chartist-js/dist/chartist-init.css" rel="stylesheet">
    <link href="bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <link href="bower_components/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet">
    <link href="bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/sweetalert.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o), m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-85622565-1', 'auto');
    ga('send', 'pageview');
    </script>
</head>

<body>
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
            <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                <div class="top-left-part">
                    <a class="logo" href="index.html"><b><img src="images/maple-icon.png" alt="Home"></b><span class="hidden-xs"><img src="images/maple-admin.png" alt="Home"></span></a>
                </div>
                <ul class="nav navbar-top-links navbar-left">
                    <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
                </ul>
                <ul id="menu" class="nav navbar-top-links navbar-right pull-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="images/users/hritik.jpg" alt="user-img" width="36" class="img-circle"><b class="hidden-xs">Wrappixel</b> </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#"><i class="ti-user"></i> My Profile</a></li>
                            <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
                            <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#"><i class="fa fa-power-off"></i> Logout</a></li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <div class="navbar-default sidebar nicescroll" role="navigation">
            <div class="sidebar-nav navbar-collapse ">
                <ul class="nav" id="side-menu">
                    <li class="nav-small-cap">Menu Principal</li>
                    <li> 
                        <a href="#/dashboard" class="waves-effect"><i class="icon-speedometer fa-fw"></i> Dashboard</a>
                        <a href="#/usuarios" class="waves-effect"><i class="fa fa-user fa-fw"></i> Usuarios</a>
                        <a href="#/pacientes" class="waves-effect"><i class="fa fa-users fa-fw"></i> Pacientes</a>
                        <a href="#/sucursales" class="waves-effect"><i class="fa fa-building-o fa-fw"></i> Sucursales</a>
                        <a href="#/centroemergencia" class="waves-effect"><i class="fa fa-hospital-o fa-fw"></i> Centro Emergencia</a>
                    </li>
                    <li> 
                        <a href="#" class="waves-effect"><i class="icon-layers fa-fw"></i> Configuraciones<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="#/tipotelefono">Tipo Teléfono</a></li>
                            <li><a href="#/tipocorreo">Tipo Correo</a></li>
                            <li><a href="#/tipodireccion">Tipo Direccion</a></li>
                            <li><a href="#/tipocontacto">Tipo Contacto</a></li>
                            <li><a href="#/tipoprofesion">Tipo Profesiones</a></li>
                            <li><a href="#/tipopaciente">Tipo Paciente</a></li>
                            <li><a href="#/tiposangre">Tipo Sangre</a></li>
                            <li><a href="#/tipoenfermedad">Tipo Enfermedad</a></li>
                            <li><a href="#/paises">Paises</a></li>
                            <li><a href="#/formapago">Formas de Pago</a></li>
                            <li><a href="#/motivoconsulta">Motivo Consulta</a></li>
                            <li><a href="#/mediopublicitario">Medio Publicitario</a></li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div id="contenido-dinamico"></div>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
        <footer class="footer text-center"> 2017 &copy; Grupo Dent </footer>
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
    <!--Nice scroll JavaScript -->
    <script src="js/jquery.nicescroll.js"></script>
    <script src="bower_components/datatables/jquery.dataTables.min.js"></script>
    <script src="bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="js/myadmin.js"></script>
    <script src="js/sweetalert.min.js"></script>
    <script src="js/sammy.js"></script>
    <script type="text/javascript">
        var ratPack = jQuery.sammy( function( e ){
            this.element_selector = "#contenido-dinamico";

            this.get("#/", function( context ){
                  context.app.swap("");
                  context.$element().load("layouts/dashboard", function(){});
                  jQuery("#side-menu").find(".active").removeClass("active");
            });

            this.get("#/dashboard", function( context ){
                  context.app.swap("");
                  context.$element().load("layouts/dashboard", function(){});
                  jQuery("#side-menu").find(".active").removeClass("active");
            });

            this.get("#/usuarios", function( context ){
                  context.app.swap("");
                  context.$element().load("layouts/usuarios", function(){});
                  jQuery("#side-menu").find(".active").removeClass("active");
            });

            this.get("#/pacientes", function( context ){
                  context.app.swap("");
                  context.$element().load("layouts/pacientes", function(){});
                  jQuery("#side-menu").find(".active").removeClass("active");
            });

            this.get("#/sucursales", function( context ){
                  context.app.swap("");
                  context.$element().load("layouts/sucursales", function(){});
                  jQuery("#side-menu").find(".active").removeClass("active");
            });

            this.get("#/centroemergencia", function( context ){
                  context.app.swap("");
                  context.$element().load("layouts/centroemergencia", function(){});
                  jQuery("#side-menu").find(".active").removeClass("active");
            });

            this.get("#/tipotelefono", function( context ){
                  context.app.swap("");
                  context.$element().load("layouts/tipotelefono", function(){});
                  jQuery("#side-menu").find(".active").removeClass("active");
            });

            this.get("#/tipocorreo", function( context ){
                  context.app.swap("");
                  context.$element().load("layouts/tipocorreo", function(){});
                  jQuery("#side-menu").find(".active").removeClass("active");
            });

            this.get("#/tipodireccion", function( context ){
                  context.app.swap("");
                  context.$element().load("layouts/tipodireccion", function(){});
                  jQuery("#side-menu").find(".active").removeClass("active");
            });

            this.get("#/tipocontacto", function( context ){
                  context.app.swap("");
                  context.$element().load("layouts/tipocontacto", function(){});
                  jQuery("#side-menu").find(".active").removeClass("active");
            });

            this.get("#/tipoprofesion", function( context ){
                  context.app.swap("");
                  context.$element().load("layouts/tipoprofesion", function(){});
                  jQuery("#side-menu").find(".active").removeClass("active");
            });

            this.get("#/tipopaciente", function( context ){
                  context.app.swap("");
                  context.$element().load("layouts/tipopaciente", function(){});
                  jQuery("#side-menu").find(".active").removeClass("active");
            });

            this.get("#/tiposangre", function( context ){
                  context.app.swap("");
                  context.$element().load("layouts/tiposangre", function(){});
                  jQuery("#side-menu").find(".active").removeClass("active");
            });

            this.get("#/tipoenfermedad", function( context ){
                  context.app.swap("");
                  context.$element().load("layouts/tipoenfermedad", function(){});
                  jQuery("#side-menu").find(".active").removeClass("active");
            });

            this.get("#/paises", function( context ){
                  context.app.swap("");
                  context.$element().load("layouts/paises", function(){});
                  jQuery("#side-menu").find(".active").removeClass("active");
            });

            this.get("#/formapago", function( context ){
                  context.app.swap("");
                  context.$element().load("layouts/formapago", function(){});
                  jQuery("#side-menu").find(".active").removeClass("active");
            });

            this.get("#/motivoconsulta", function( context ){
                  context.app.swap("");
                  context.$element().load("layouts/motivoconsulta", function(){});
                  jQuery("#side-menu").find(".active").removeClass("active");
            });

            this.get("#/mediopublicitario", function( context ){
                  context.app.swap("");
                  context.$element().load("layouts/mediopublicitario", function(){});
                  jQuery("#side-menu").find(".active").removeClass("active");
            });

            this.notFound = function( context, url ){
                  console.log("URL No Encontrada");
            }
        });

        jQuery( function(){
            ratPack.run("#/");
        });

    </script>
</body>

</html>

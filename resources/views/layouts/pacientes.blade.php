<div class="row bg-title">
    <div class="col-lg-12">
        <h4 class="page-title">Pacientes</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <h3>Lista</h3>
            <p class="m-t-20">
                <button type="button" class="btn btn-info btn-crear">Crear Nuevo</button>            
            </p>
            <table id="tabla-registros" class="table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombres</th>
                        <th>Apellidos</th>
                        <th>Acciones</th>                        
                    </tr>
                </thead>    
            </table>
        </div>
    </div>
</div>

<div class="row hide" id="block-paciente">
    <div class="col-sm-12">
        <div class="panel panel-info">
            <div class="panel-heading"> Datos Paciente
                <div class="pull-right">
                    <a href="#" class="close-paciente"><i class="ti-close"></i></a>
                    <input type="hidden" id="personaid">
                    <input type="hidden" id="pacienteid">
                </div>
            </div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <div class="white-box">
                                <!-- Nav tabs -->
                                <ul class="nav customtab nav-tabs" role="tablist">
                                    <li role="presentation" class="lidg active"><a href="#datosgenerales" aria-controls="home" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Datos Generales</span></a></li>
                                    <li role="presentation" class="lidir"><a class="tabdir" href="#direcciones" aria-controls="profile" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Direcciones</span></a></li>
                                    <li role="presentation" class="litel"><a class="tabtel" href="#telefonos" aria-controls="messages" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-email"></i></span> <span class="hidden-xs">Telefonos</span></a></li>
                                    <li role="presentation" class="licor"><a class="tabcor" href="#correos" aria-controls="settings" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-settings"></i></span> <span class="hidden-xs">Correos</span></a></li>
                                    <li role="presentation" class="lifac"><a class="tabfac" href="#facturacion" aria-controls="settings" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-settings"></i></span> <span class="hidden-xs">Facturación</span></a></li>
                                    <li role="presentation" class="lidpa"><a class="tabdpa" href="#datospaciente" aria-controls="settings" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-settings"></i></span> <span class="hidden-xs">Datos Paciente</span></a></li>
                                    <li role="presentation" class="lihme"><a class="tabhme" href="#historiamedica" aria-controls="settings" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-settings"></i></span> <span class="hidden-xs">Historia Médica</span></a></li>
                                    <li role="presentation" class="lihod"><a class="tabhod" href="#historiaodontologica" aria-controls="settings" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-settings"></i></span> <span class="hidden-xs">Historia Odontológica</span></a></li>
                                    <li role="presentation" class="lienf"><a class="tabenf" href="#enfermedades" aria-controls="settings" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-settings"></i></span> <span class="hidden-xs">Enfermedades</span></a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <!-- Datos Generales -->
                                    <div role="tabpanel" class="tab-pane fade in active" id="datosgenerales">
                                        <form id="form-datosgenerales">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Nombres:</label>
                                                            <input type="text" class="form-control" name="nombres" id="nombres">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Apellidos:</label>
                                                            <input type="text" class="form-control" name="apellidos" id="apellidos">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Apellido Casada:</label>
                                                            <input type="text" class="form-control" name="casada" id="casada">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Fecha Nacimiento:</label>
                                                            <input type="text" class="form-control mydatepicker" placeholder="mm/dd/yyyy" name="nacimiento" id="nacimiento">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 pull-right">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Documento Identifación:</label>
                                                            <input type="text" class="form-control" name="dni" id="dni">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Estado Civil:</label>
                                                            <select id="estadocivil" name="estadocivil" class="form-control">
                                                                <option value="S">Soltero</option>
                                                                <option value="C">Casado</option>
                                                                <option value="V">Viudo</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Genero:</label>
                                                            <select id="genero" name="genero" class="form-control">
                                                                <option value="M">Masculino</option>
                                                                <option value="F">Femenino</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Profesión:</label>
                                                            <select class="form-control" id="profesion" name="profesion">
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <p class="m-t-20">
                                                <button type="submit" class="btn btn-info" id="btn-fdg">Crear Nuevo</button>            
                                            </p>
                                        </form>
                                        <div class="clearfix"></div>
                                    </div>
                                    <!-- Direcciones -->
                                    <div role="tabpanel" class="tab-pane fade" id="direcciones">
                                        <div class="row">
                                            <form id="form-direcciones">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Pais:</label>
                                                                <select id="pais" name="pais" class="form-control">
                                                                </select>
                                                                <input type="hidden" id="updateid">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Departamento:</label>
                                                                <select id="departamento" name="departamento" class="form-control">
                                                                    <option value="0">Sin Seleccionar</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Municipio:</label>
                                                                <select id="municipio" name="municipio" class="form-control">
                                                                    <option value="0">Sin Seleccionar</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <p class="m-t-20">
                                                        <button type="submit" class="btn btn-info btn-fdir">Agregar Dirección</button>
                                                        <button type="button" class="btn btn-danger btn-cdir hide">Cancelar</button>
                                                    </p>
                                                </div>
                                                <div class="col-md-6 pull-right">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label">Dirección:</label>
                                                                <input type="text" class="form-control" name="direccion" id="direccion">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Tipo Dirección:</label>
                                                                <select class="form-control" id="tipodireccion" name="tipodireccion">
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Preferida:</label>
                                                                <select class="form-control" id="preferida" name="preferida">
                                                                    <option value="0">No</option>
                                                                    <option value="1">Si</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="white-box">
                                                    <table id="tabla-direcciones" class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Dirección</th>
                                                                <th>Pais</th>
                                                                <th>Departamento</th>
                                                                <th>Municipio</th>
                                                                <th>Tipo Dirección</th>
                                                                <th>Preferida</th>
                                                                <th>Acciones</th>                        
                                                            </tr>
                                                        </thead>    
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <!-- Telefonos -->
                                    <div role="tabpanel" class="tab-pane fade" id="telefonos">
                                        <div class="row">
                                            <form id="form-telefonos">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Numero:</label>
                                                                <input type="text" class="form-control" name="numero" id="numero">
                                                                <input type="hidden" id="updateid">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Tipo Telefono:</label>
                                                                <select class="form-control" id="tipotelefono" name="tipotelefono">
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Preferida:</label>
                                                                <select class="form-control" id="preferida" name="preferida">
                                                                    <option value="0">No</option>
                                                                    <option value="1">Si</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                     <p class="m-t-20">
                                                        <button type="submit" class="btn btn-info btn-ftel">Agregar Teléfono</button>
                                                        <button type="button" class="btn btn-danger btn-ctel hide">Cancelar</button>          
                                                    </p>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="white-box">
                                                    <table id="tabla-telefonos" class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Teléfono</th>
                                                                <th>Tipo Teléfono</th>
                                                                <th>Preferida</th>
                                                                <th>Acciones</th>                        
                                                            </tr>
                                                        </thead>    
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <!-- Correos -->
                                    <div role="tabpanel" class="tab-pane fade" id="correos">
                                        <div class="row">
                                            <form id="form-correos">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Correo:</label>
                                                                <input type="text" class="form-control" name="correo" id="correo">
                                                                <input type="hidden" id="updateid">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Tipo Correo:</label>
                                                                <select class="form-control" id="tipocorreo" name="tipocorreo">
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Preferida:</label>
                                                                <select class="form-control" id="preferida" name="preferida">
                                                                    <option value="0">No</option>
                                                                    <option value="1">Si</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <p class="m-t-20">
                                                        <button type="submit" class="btn btn-info btn-fcor">Agregar Correo</button>
                                                        <button type="button" class="btn btn-danger btn-ccor hide">Cancelar</button>          
                                                    </p>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="white-box">
                                                    <table id="tabla-correos" class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Correo</th>
                                                                <th>Tipo Correo</th>
                                                                <th>Preferida</th>
                                                                <th>Acciones</th>                        
                                                            </tr>
                                                        </thead>    
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <!-- Facturacion -->
                                    <div role="tabpanel" class="tab-pane fade" id="facturacion">
                                        <div class="row">
                                            <form id="form-facturacion">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Nombre:</label>
                                                                <input type="text" class="form-control" name="nombre" id="nombre">
                                                                <input type="hidden" id="updateid">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Nit:</label>
                                                                <input type="text" class="form-control" name="nit" id="nit">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Preferida:</label>
                                                                <select class="form-control" id="preferida" name="preferida">
                                                                    <option value="0">No</option>
                                                                    <option value="1">Si</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Direccion:</label>
                                                                <input type="text" class="form-control" name="direccion" id="direccion">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <p class="m-t-20">
                                                        <button type="submit" class="btn btn-info btn-ffac">Agregar Facturación</button>
                                                        <button type="button" class="btn btn-danger btn-cfac hide">Cancelar</button>
                                                    </p>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="white-box">
                                                    <table id="tabla-facturacion" class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Nombre</th>
                                                                <th>Nit</th>
                                                                <th>Direccion</th>
                                                                <th>Preferida</th>
                                                                <th>Acciones</th>                        
                                                            </tr>
                                                        </thead>    
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <!-- Datos Paciente -->
                                    <div role="tabpanel" class="tab-pane fade" id="datospaciente">
                                        <div class="row">
                                            <form id="form-datospaciente">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Motivo Consulta:</label>
                                                                <select class="form-control" id="motivoconsulta" name="motivoconsulta">
                                                                </select>
                                                                <input type="hidden" id="updateid">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">En Caso de Emergencia Trasladar a:</label>
                                                                <select class="form-control" id="centroemergencia" name="centroemergencia">
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label">Aspectos Relevantes:</label>
                                                                <input type="text" class="form-control" name="aspectosrelevantes" id="aspectosrelevantes">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Tipo Paciente:</label>
                                                                <select class="form-control" id="tipopaciente" name="tipopaciente">
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Tipo Sangre:</label>
                                                                <select class="form-control" id="tiposangre" name="tiposangre">
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <p class="m-t-20">
                                                        <button type="submit" class="btn btn-info">Actualzar Datos</button>
                                                    </p>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Poliza Seguro:</label>
                                                                <input type="text" class="form-control" name="polizaseguro" id="polizaseguro">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Medio por el que se entero:</label>
                                                                <select class="form-control" id="mediopublicitario" name="mediopublicitario">
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Convenio:</label>
                                                                <select class="form-control" id="convenio" name="convenio">
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Monto Autorizado:</label>
                                                                <input type="text" class="form-control" name="montoautorizado" id="montoautorizado">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <!-- Historia Medica -->
                                    <div role="tabpanel" class="tab-pane fade" id="historiamedica">
                                        <div class="row">
                                            <form id="form-historiamedica">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label">¿Ha tenido problemas de salud en el último año? ¿Cuál(es)?</label>
                                                                <input type="text" class="form-control" name="hmproblemas" id="hmproblemas">
                                                                <input type="hidden" id="updateid">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label">¿Le han realizado una cirugía grave? ¿Cuál(es)?</label>
                                                                <input type="text" class="form-control" name="hmcirugias" id="hmcirugias">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">¿Ha padecido o padece de algún trastorno del corazón?</label>
                                                                <select class="form-control" id="hmtrastornocorazon" name="hmtrastornocorazon">
                                                                    <option value="0">No</option>
                                                                    <option value="1">Si</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">¿Cuál(es)?</label>
                                                                <br/>
                                                                <br/>
                                                                <input type="text" class="form-control" name="hmtrastornocorazoncomentario" id="hmtrastornocorazoncomentario">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label">¿Toma regularmente medicamentos, incluyendo los no prescritos por un Tratante?</label>
                                                                <input type="text" class="form-control" name="hmmedicamentos" id="hmmedicamentos">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <p class="m-t-20">
                                                        <button type="submit" class="btn btn-info btn-ffac">Actualzar Datos</button>
                                                    </p>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">¿Fuma?</label>
                                                                <select class="form-control" id="hmfuma" name="hmfuma">
                                                                    <option value="0">No</option>
                                                                    <option value="1">Si</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">¿Ingiere bebidas alcohólicas con regularidad?</label>
                                                                <select class="form-control" id="hmbebidas" name="hmbebidas">
                                                                    <option value="0">No</option>
                                                                    <option value="1">Si</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label">¿Se encuentra actualmente bajo algún tratamiento Tratante?</label>
                                                                <input type="text" class="form-control" name="hmtratamiento" id="hmtratamiento">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label">¿Es alérgico(a) a algún medicamento, anestésico, almimento, sustancia quimia, otros? ¿A cuál(es)?</label>
                                                                <input type="text" class="form-control" name="hmalergico" id="hmalergico">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label">¿Ha sufrido accidentes?</label>
                                                                <input type="text" class="form-control" name="hmaccidentes" id="hmaccidentes">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label">Comentarios Relevantes</label>
                                                                <input type="text" class="form-control" name="hmcomentarios" id="hmcomentarios">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <!-- Historia Odontologica -->
                                    <div role="tabpanel" class="tab-pane fade" id="historiaodontologica">
                                        <div class="row">
                                            <form id="form-historiaodontologica">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label">¿Hace cuánto tiempo fue su última visita al dentista?</label>
                                                                <input type="text" class="form-control" name="houltimavisita" id="houltimavisita">
                                                                <input type="hidden" id="updateid">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label">¿Anteriormente ha estado en tratamiento dental? ¿Qué le fue realizado?</label>
                                                                <input type="text" class="form-control" name="hotratamientodental" id="hotratamientodental">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label">Comentarios relevantes y observaciones de historia odontológica</label>
                                                                <input type="text" class="form-control" name="hocomentariosrelevantes" id="hocomentariosrelevantes">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <p class="m-t-20">
                                                        <button type="submit" class="btn btn-info btn-ffac">Actualzar Datos</button>
                                                    </p>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label">¿Actualmente tiene dolor o molestias en alguna pieza dental?</label>
                                                                <select class="form-control" id="homolestiaactual" name="homolestiaactual">
                                                                    <option value="0">No</option>
                                                                    <option value="1">Si</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label">¿Ha sufrido algún accidente que involucre a sus dientes o su mandibula?</label>
                                                                <select class="form-control" id="hoaccidentes" name="hoaccidentes">
                                                                    <option value="0">No</option>
                                                                    <option value="1">Si</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label">¿Le han extraido piezas dentales?</label>
                                                                <select class="form-control" id="hoextraccion" name="hoextraccion">
                                                                    <option value="0">No</option>
                                                                    <option value="1">Si</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <!-- Enfermedades -->
                                    <div role="tabpanel" class="tab-pane fade" id="enfermedades">
                                        <div class="row">
                                            <form id="form-enfermedades">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Enfermedad:</label>
                                                                <select class="form-control" id="tipoenfermedad" name="tipoenfermedad">
                                                                </select>
                                                                <input type="hidden" id="updateid">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <p class="m-t-20">
                                                        <button type="submit" class="btn btn-info btn-ffac">Añadir Enfermedad</button>
                                                    </p>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="white-box">
                                                    <table id="tabla-enfermedades" class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Enfermedad</th>
                                                                <th>Acciones</th>                        
                                                            </tr>
                                                        </thead>    
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" src="js/pacientes.js"></script>
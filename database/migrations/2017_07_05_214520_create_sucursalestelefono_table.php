<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSucursalestelefonoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('SucursalesTelefono', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero');
            $table->boolean('preferida');
            $table->integer('sucursal')->unsigned();
            $table->foreign('sucursal')->references('id')->on('Sucursales');
            $table->integer('tipotelefono')->unsigned();
            $table->foreign('tipotelefono')->references('id')->on('TipoTelefono');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('SucursalesTelefono');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCentroemergenciatelefonoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CentroEmergenciaTelefono', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero');
            $table->boolean('preferida');
            $table->integer('centroemergencia')->unsigned();
            $table->foreign('centroemergencia')->references('id')->on('CentroEmergencia');
            $table->integer('tipotelefono')->unsigned();
            $table->foreign('tipotelefono')->references('id')->on('TipoTelefono');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('CentroEmergenciaTelefono');
    }
}

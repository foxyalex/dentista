<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacienteenfermedadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PacienteEnfermedad', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('paciente')->unsigned();
            $table->foreign('paciente')->references('id')->on('Paciente');
            $table->integer('tipoenfermedad')->unsigned();
            $table->foreign('tipoenfermedad')->references('id')->on('TipoEnfermedad');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PacienteEnfermedad');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCentroemergenciacorreoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CentroEmergenciaCorreo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('correo');
            $table->boolean('preferida');
            $table->integer('centroemergencia')->unsigned();
            $table->foreign('centroemergencia')->references('id')->on('CentroEmergencia');
            $table->integer('tipocorreo')->unsigned();
            $table->foreign('tipocorreo')->references('id')->on('TipoCorreo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('CentroEmergenciaCorreo');
    }
}

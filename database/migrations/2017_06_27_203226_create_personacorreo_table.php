<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonacorreoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PersonaCorreo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('correo');
            $table->boolean('preferida');
            $table->integer('persona')->unsigned();
            $table->foreign('persona')->references('id')->on('Persona');
            $table->integer('tipocorreo')->unsigned();
            $table->foreign('tipocorreo')->references('id')->on('TipoCorreo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PersonaCorreo');
    }
}

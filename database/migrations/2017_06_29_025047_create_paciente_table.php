<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacienteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Paciente', function (Blueprint $table) {
            $table->increments('id');
            $table->string('aspectosrelevantes')->default('')->nullable();
            $table->string('polizaseguro')->default('')->nullable();
            $table->float('montoautorizado')->default(0.0)->nullable();
            
            $table->string('hmproblemas')->default('')->nullable();
            $table->string('hmcirugias')->default('')->nullable();
            $table->integer('hmtrastornocorazon')->default(0);
            $table->string('hmtrastornocorazoncomentario')->default('')->nullable();
            $table->string('hmmedicamentos')->default('')->nullable();
            $table->string('hmfuma')->default('');
            $table->string('hmbebidas')->default('');
            $table->string('hmtratamiento')->default('')->nullable();
            $table->string('hmalergico')->default('')->nullable();
            $table->string('hmaccidentes')->default('')->nullable();
            $table->string('hmcomentarios')->default('')->nullable();

            $table->string('houltimavisita')->default('')->nullable();
            $table->string('hotratamientodental')->default('')->nullable();
            $table->string('hocomentariosrelevantes')->default('')->nullable();
            $table->integer('homolestiaactual')->default(0);
            $table->integer('hoaccidentes')->default(0)->nullable();
            $table->integer('hoextraccion')->default(0)->nullable();

            //RELACIONES
            $table->integer('persona')->unsigned();
            $table->foreign('persona')->references('id')->on('Persona');
            $table->integer('motivoconsulta')->unsigned()->default(1);
            $table->foreign('motivoconsulta')->references('id')->on('MotivoConsulta');
            $table->integer('centroemergencia')->unsigned()->default(1);
            $table->foreign('centroemergencia')->references('id')->on('CentroEmergencia');
            $table->integer('tipopaciente')->unsigned()->default(1);
            $table->foreign('tipopaciente')->references('id')->on('TipoPaciente');
            $table->integer('tiposangre')->unsigned()->default(1);;
            $table->foreign('tiposangre')->references('id')->on('TipoSangre');
            $table->integer('mediopublicitario')->unsigned()->default(1);;
            $table->foreign('mediopublicitario')->references('id')->on('MedioPublicitario');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Paciente');
    }
}

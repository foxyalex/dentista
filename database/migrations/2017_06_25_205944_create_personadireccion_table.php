<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonadireccionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PersonaDireccion', function (Blueprint $table) {
            $table->increments('id');
            $table->string('direccion');
            $table->boolean('preferida');
            $table->integer('pais')->unsigned();
            $table->foreign('pais')->references('id')->on('Paises');
            $table->integer('departamento')->unsigned();
            $table->foreign('departamento')->references('id')->on('Departamentos');
            $table->integer('municipio')->unsigned();
            $table->foreign('municipio')->references('id')->on('Municipios');
            $table->integer('persona')->unsigned();
            $table->foreign('persona')->references('id')->on('Persona');
            $table->integer('tipodireccion')->unsigned();
            $table->foreign('tipodireccion')->references('id')->on('TipoDireccion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PersonaDireccion');
    }
}

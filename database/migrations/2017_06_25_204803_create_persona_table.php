<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Persona', function (Blueprint $table) {
            $table->increments('id');
            $table->string('genero');
            $table->string('estadocivil');
            $table->string('nombres');
            $table->string('apellidos');
            $table->string('casada')->default('')->nullable();
            $table->date('nacimiento');
            $table->string('dni');

            $table->integer('profesion')->unsigned();
            $table->foreign('profesion')->references('id')->on('TipoProfesion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Persona');
    }
}

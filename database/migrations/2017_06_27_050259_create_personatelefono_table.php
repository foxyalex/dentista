<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonatelefonoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PersonaTelefono', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero');
            $table->boolean('preferida');
            $table->integer('persona')->unsigned();
            $table->foreign('persona')->references('id')->on('Persona');
            $table->integer('tipotelefono')->unsigned();
            $table->foreign('tipotelefono')->references('id')->on('TipoTelefono');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PersonaTelefono');
    }
}

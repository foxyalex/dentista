<?php

use Illuminate\Database\Seeder;

class CentroEmergenciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('CentroEmergencia')->insert([
            'nombre' => 'Sin Asignar',
            'estado' => true,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('CentroEmergencia')->insert([
            'nombre' => 'Hospital Roosvelt',
            'estado' => true,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('CentroEmergencia')->insert([
            'nombre' => 'Centro Medico',
            'estado' => true,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
    }
}

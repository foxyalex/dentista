<?php

use Illuminate\Database\Seeder;

class SucursalesCorreoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('SucursalesCorreo')->insert([
            'correo' => 'admin@dentistas.org',
            'preferida' => true,
            'sucursal' => 1,
            'tipocorreo' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('SucursalesCorreo')->insert([
            'correo' => 'informacion@dentistas.org',
            'preferida' => false,
            'sucursal' => 1,
            'tipocorreo' => 2,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('SucursalesCorreo')->insert([
            'correo' => 'contacto@dentistas.org',
            'preferida' => false,
            'sucursal' => 2,
            'tipocorreo' => 2,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
    }
}

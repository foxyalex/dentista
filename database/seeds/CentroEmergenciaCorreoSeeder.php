<?php

use Illuminate\Database\Seeder;

class CentroEmergenciaCorreoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('CentroEmergenciaCorreo')->insert([
            'correo' => 'test@test.com',
            'preferida' => true,
            'centroemergencia' => 2,
            'tipocorreo' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('CentroEmergenciaCorreo')->insert([
            'correo' => 'test2@test.com',
            'preferida' => false,
            'centroemergencia' => 2,
            'tipocorreo' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('CentroEmergenciaCorreo')->insert([
            'correo' => 'hospital@hospital.com',
            'preferida' => true,
            'centroemergencia' => 3,
            'tipocorreo' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
    }
}

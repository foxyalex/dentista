<?php

use Illuminate\Database\Seeder;

class PersonaTelefonoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('PersonaTelefono')->insert([
            'numero' => '30358525',
            'preferida' => true,
            'persona' => 1,
            'tipotelefono' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('PersonaTelefono')->insert([
            'numero' => '41283502',
            'preferida' => false,
            'persona' => 1,
            'tipotelefono' => 2,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class PacienteEnfermedadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('PacienteEnfermedad')->insert([
            'paciente' => 1,
            'tipoenfermedad' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('PacienteEnfermedad')->insert([
            'paciente' => 1,
            'tipoenfermedad' => 2,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
    }
}

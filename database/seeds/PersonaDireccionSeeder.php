<?php

use Illuminate\Database\Seeder;

class PersonaDireccionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('PersonaDireccion')->insert([
            'direccion' => '5ta Calle A 1-31 Zona 3',
            'preferida' => true,
            'pais' => 1,
            'departamento' => 20,
            'municipio' => 3,
            'persona' => 1,
            'tipodireccion' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
    }
}

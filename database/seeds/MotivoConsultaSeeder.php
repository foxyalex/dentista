<?php

use Illuminate\Database\Seeder;

class MotivoConsultaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('MotivoConsulta')->insert([
            'nombre' => 'Primera Consulta',
            'estado' => true,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('MotivoConsulta')->insert([
            'nombre' => 'Re-Consulta',
            'estado' => true,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
    }
}

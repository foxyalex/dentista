<?php

use Illuminate\Database\Seeder;

class DepartamentosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Departamentos')->insert([
            'nombre' => 'Alta Verapaz',
            'pais' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Departamentos')->insert([
            'nombre' => 'Baja Verapaz',
            'pais' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Departamentos')->insert([
            'nombre' => 'Chimaltenango',
            'pais' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Departamentos')->insert([
            'nombre' => 'Chiquimula',
            'pais' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Departamentos')->insert([
            'nombre' => 'El Progreso',
            'pais' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Departamentos')->insert([
            'nombre' => 'Escuintla',
            'pais' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Departamentos')->insert([
            'nombre' => 'Guatemala',
            'pais' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Departamentos')->insert([
            'nombre' => 'Huehuetenango',
            'pais' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Departamentos')->insert([
            'nombre' => 'Izabal',
            'pais' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Departamentos')->insert([
            'nombre' => 'Jalapa',
            'pais' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Departamentos')->insert([
            'nombre' => 'Jutiapa',
            'pais' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Departamentos')->insert([
            'nombre' => 'Petén',
            'pais' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Departamentos')->insert([
            'nombre' => 'Quetzaltenango',
            'pais' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Departamentos')->insert([
            'nombre' => 'Quiché',
            'pais' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Departamentos')->insert([
            'nombre' => 'Retalhuleu',
            'pais' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Departamentos')->insert([
            'nombre' => 'Sacatepéquez',
            'pais' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Departamentos')->insert([
            'nombre' => 'San Marcos',
            'pais' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Departamentos')->insert([
            'nombre' => 'Santa Rosa',
            'pais' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Departamentos')->insert([
            'nombre' => 'Sololá',
            'pais' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Departamentos')->insert([
            'nombre' => 'Suchitepéquez',
            'pais' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Departamentos')->insert([
            'nombre' => 'Totonicapan',
            'pais' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Departamentos')->insert([
            'nombre' => 'Zacapa',
            'pais' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class TipoProfresionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('TipoProfesion')->insert([
            'nombre' => 'Sin Profesión',
            'estado' => true,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('TipoProfesion')->insert([
            'nombre' => 'Ingeniero en Sistemas',
            'estado' => true,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('TipoProfesion')->insert([
            'nombre' => 'Abogado',
            'estado' => true,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('TipoProfesion')->insert([
            'nombre' => 'Astronomo',
            'estado' => true,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class PersonaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Persona')->insert([
            'genero' => 'M',
            'estadocivil' => 'S',
            'nombres' => 'Jorge',
            'apellidos' => 'Carcamo',
            'casada' => '',
            'nacimiento' => '1990/10/09',
            'dni' => '2798344882',
            'profesion' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Persona')->insert([
            'genero' => 'M',
            'estadocivil' => 'S',
            'nombres' => 'Alex',
            'apellidos' => 'Mejicanos',
            'casada' => '',
            'nacimiento' => '1993/02/27',
            'dni' => '2197830471001',
            'profesion' => 2,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
    }
}

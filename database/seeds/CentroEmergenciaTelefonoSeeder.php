<?php

use Illuminate\Database\Seeder;

class CentroEmergenciaTelefonoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('CentroEmergenciaTelefono')->insert([
            'numero' => '24543433',
            'preferida' => true,
            'centroemergencia' => 2,
            'tipotelefono' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('CentroEmergenciaTelefono')->insert([
            'numero' => '65473342',
            'preferida' => false,
            'centroemergencia' => 2,
            'tipotelefono' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('CentroEmergenciaTelefono')->insert([
            'numero' => '22334455',
            'preferida' => true,
            'centroemergencia' => 3,
            'tipotelefono' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
    }
}

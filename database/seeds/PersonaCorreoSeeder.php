<?php

use Illuminate\Database\Seeder;

class PersonaCorreoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('PersonaCorreo')->insert([
            'correo' => 'alexmejicanos@outlook.com',
            'preferida' => true,
            'persona' => 1,
            'tipocorreo' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('PersonaCorreo')->insert([
            'correo' => 'alexmejicanos44@gmail.com',
            'preferida' => false,
            'persona' => 1,
            'tipocorreo' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
    }
}

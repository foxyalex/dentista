<?php

use Illuminate\Database\Seeder;

class TipoDireccionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('TipoDireccion')->insert([
            'nombre' => 'Residencia',
            'estado' => true,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('TipoDireccion')->insert([
            'nombre' => 'Oficina',
            'estado' => true,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
    }
}

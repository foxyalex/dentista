<?php

use Illuminate\Database\Seeder;

class PersonaFacturacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('PersonaFacturacion')->insert([
            'nombre' => 'Alex Mejicanos',
            'direccion' => '9na Calle 2-70 Zona 1 Mazatenango',
            'nit' => '7331662-8',
            'preferida' => true,
            'persona' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('PersonaFacturacion')->insert([
            'nombre' => 'Sergio Hernandez',
            'direccion' => 'Ciudad',
            'nit' => 'C/F',
            'preferida' => false,
            'persona' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class MunicipiosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Municipios')->insert([
            'nombre' => 'Cahabón',
            'departamento' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Chahal',
            'departamento' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Chisec',
            'departamento' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Cobán',
            'departamento' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Fray Bartolomé de las Casas',
            'departamento' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Lanquín',
            'departamento' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Panzós',
            'departamento' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Raxruha',
            'departamento' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Cristóbal Verapaz',
            'departamento' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Juan Chamelco',
            'departamento' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Pedro Carchá',
            'departamento' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santa Cruz Verapaz',
            'departamento' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Senahú',
            'departamento' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Tactic',
            'departamento' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Tamahú',
            'departamento' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Tucurú',
            'departamento' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santa Catarina La Tinta',
            'departamento' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Cubulco',
            'departamento' => 2,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
        
        DB::table('Municipios')->insert([
            'nombre' => 'Granados',
            'departamento' => 2,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
        
        DB::table('Municipios')->insert([
            'nombre' => 'Purulhá',
            'departamento' => 2,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Rabinal',
            'departamento' => 2,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Salamá',
            'departamento' => 2,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Jerónimo',
            'departamento' => 2,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Miguel Chicaj',
            'departamento' => 2,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santa Cruz El Chol',
            'departamento' => 2,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Acatenango',
            'departamento' => 3,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Chimaltenango',
            'departamento' => 3,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'El Tejar',
            'departamento' => 3,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Parramos',
            'departamento' => 3,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Patzicía',
            'departamento' => 3,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Patzún',
            'departamento' => 3,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Pochuta',
            'departamento' => 3,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Andrés Itzapa',
            'departamento' => 3,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San José Poaquil',
            'departamento' => 3,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Juan Comalapa',
            'departamento' => 3,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Martín Jilotepeque',
            'departamento' => 3,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santa Apolonia',
            'departamento' => 3,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santa Cruz Balanyá',
            'departamento' => 3,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Tecpán Guatemala',
            'departamento' => 3,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Yepocapa',
            'departamento' => 3,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Zaragoza',
            'departamento' => 3,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Camotán',
            'departamento' => 4,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Chiquimula',
            'departamento' => 4,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Concepción Las Minas',
            'departamento' => 4,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Esquipulas',
            'departamento' => 4,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Ipala',
            'departamento' => 4,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Jocotán',
            'departamento' => 4,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Olopa',
            'departamento' => 4,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Quezaltepeque',
            'departamento' => 4,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Jacinto',
            'departamento' => 4,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San José La Arada',
            'departamento' => 4,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Juan Ermita',
            'departamento' => 4,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'El Jícaro',
            'departamento' => 5,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Guastatoya',
            'departamento' => 5,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Morazán',
            'departamento' => 5,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Agustín Acasaguastlán',
            'departamento' => 5,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Antonio La Paz',
            'departamento' => 5,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Cristóbal Acasaguastlán',
            'departamento' => 5,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Sanarate',
            'departamento' => 5,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Escuintla',
            'departamento' => 6,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Guanagazapa',
            'departamento' => 6,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Iztapa',
            'departamento' => 6,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'La Democracia',
            'departamento' => 6,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'La Gomera',
            'departamento' => 6,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Masagua',
            'departamento' => 6,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Nueva Concepción',
            'departamento' => 6,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Palín',
            'departamento' => 6,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San José',
            'departamento' => 6,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Vicente Pacaya',
            'departamento' => 6,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santa Lucía Cotzumalguap',
            'departamento' => 6,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Siquinalá',
            'departamento' => 6,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Tiquisate',
            'departamento' => 6,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Amatitlán',
            'departamento' => 7,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Chinautla',
            'departamento' => 7,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Chuarrancho',
            'departamento' => 7,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Fraijanes',
            'departamento' => 7,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Ciudad Guatemala',
            'departamento' => 7,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Mixco',
            'departamento' => 7,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Palencia',
            'departamento' => 7,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Petapa',
            'departamento' => 7,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San José del Golfo',
            'departamento' => 7,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San José Pinula',
            'departamento' => 7,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Juan Sacatepéquez',
            'departamento' => 7,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Pedro Ayampuc',
            'departamento' => 7,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Pedro Sacatepéquez',
            'departamento' => 7,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Raymundo',
            'departamento' => 7,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santa Catarina Pinula',
            'departamento' => 7,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Villa Canales',
            'departamento' => 7,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Aguacatán',
            'departamento' => 8,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Chiantla',
            'departamento' => 8,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Colotenango',
            'departamento' => 8,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Concepción Huista',
            'departamento' => 8,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Cuilco',
            'departamento' => 8,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Huehuetenango',
            'departamento' => 8,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Ixtahuacán',
            'departamento' => 8,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Jacaltenango',
            'departamento' => 8,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'La Democracia',
            'departamento' => 8,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'La Libertad',
            'departamento' => 8,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Malacatancito',
            'departamento' => 8,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Nentón',
            'departamento' => 8,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Antonio Huista',
            'departamento' => 8,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Gaspar Ixchil',
            'departamento' => 8,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Juan Atitán',
            'departamento' => 8,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Juan Ixcoy',
            'departamento' => 8,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Mateo Ixtatán',
            'departamento' => 8,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Miguel Acatán',
            'departamento' => 8,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Pedro Necta',
            'departamento' => 8,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Rafael La Independencia',
            'departamento' => 8,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Rafael Petzal',
            'departamento' => 8,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Sebastián Coatán',
            'departamento' => 8,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Sebastián Huehuetenango',
            'departamento' => 8,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santa Ana Huista',
            'departamento' => 8,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santa Bárbara',
            'departamento' => 8,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santa Cruz Barillas',
            'departamento' => 8,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santa Eulalia',
            'departamento' => 8,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santiago Chimaltenango',
            'departamento' => 8,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Soloma',
            'departamento' => 8,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Tectitán',
            'departamento' => 8,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Todos Santos Cuchumatan',
            'departamento' => 8,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'El Estor',
            'departamento' => 9,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Livingston',
            'departamento' => 9,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Los Amates',
            'departamento' => 9,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Morales',
            'departamento' => 9,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Puerto Barrios',
            'departamento' => 9,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Jalapa',
            'departamento' => 10,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Mataquescuintla',
            'departamento' => 10,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Monjas',
            'departamento' => 10,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Carlos Alzatate',
            'departamento' => 10,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Luis Jilotepeque',
            'departamento' => 10,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Pedro Pinula',
            'departamento' => 10,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Manuel Chaparrón',
            'departamento' => 10,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Agua Blanca',
            'departamento' => 11,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Asunción Mita',
            'departamento' => 11,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Atescatempa',
            'departamento' => 11,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Comapa',
            'departamento' => 11,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Conguaco',
            'departamento' => 11,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'El Adelanto',
            'departamento' => 11,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'El Progreso',
            'departamento' => 11,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Jalpatagua',
            'departamento' => 11,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Jerez',
            'departamento' => 11,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Jutiapa',
            'departamento' => 11,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Moyuta',
            'departamento' => 11,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Pasaco',
            'departamento' => 11,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Quezada',
            'departamento' => 11,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San José Acatempa',
            'departamento' => 11,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santa Catarina Mita',
            'departamento' => 11,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Yupiltepeque',
            'departamento' => 11,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Zapotitlán',
            'departamento' => 11,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Dolores',
            'departamento' => 12,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Flores',
            'departamento' => 12,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'La Libertad',
            'departamento' => 12,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Melchor de Mencos',
            'departamento' => 12,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Poptún',
            'departamento' => 12,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Andrés',
            'departamento' => 12,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Benito',
            'departamento' => 12,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Francisco',
            'departamento' => 12,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San José',
            'departamento' => 12,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Luis',
            'departamento' => 12,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santa Ana',
            'departamento' => 12,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Sayaxché',
            'departamento' => 12,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Las Cruces',
            'departamento' => 12,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Almolonga',
            'departamento' => 13,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Cabricán',
            'departamento' => 13,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Cajolá',
            'departamento' => 13,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Cantel',
            'departamento' => 13,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Coatepeque',
            'departamento' => 13,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Colomba',
            'departamento' => 13,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Concepción Chiquirichapa',
            'departamento' => 13,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'El Palmar',
            'departamento' => 13,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Flores Costa Cuca',
            'departamento' => 13,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Génova',
            'departamento' => 13,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Huitán',
            'departamento' => 13,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'La Esperanza',
            'departamento' => 13,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Olintepeque',
            'departamento' => 13,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Ostuncalco',
            'departamento' => 13,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Palestina de Los Altos',
            'departamento' => 13,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Quetzaltenango',
            'departamento' => 13,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Salcajá',
            'departamento' => 13,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Carlos Sija',
            'departamento' => 13,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Francisco La Unión',
            'departamento' => 13,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Martín Sacatepéquez',
            'departamento' => 13,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Mateo',
            'departamento' => 13,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Miguel Sigüilá',
            'departamento' => 13,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Sibilia',
            'departamento' => 13,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Zunil',
            'departamento' => 13,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Canillá',
            'departamento' => 14,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Chajul',
            'departamento' => 14,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Chicamán',
            'departamento' => 14,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Chiché',
            'departamento' => 14,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Chichicastenango',
            'departamento' => 14,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Chinique',
            'departamento' => 14,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Cunén',
            'departamento' => 14,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Ixcán',
            'departamento' => 14,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Joyabaj',
            'departamento' => 14,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Nebaj',
            'departamento' => 14,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Pachalum',
            'departamento' => 14,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Patzité',
            'departamento' => 14,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Sacapulas',
            'departamento' => 14,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Andrés Sajcabajá',
            'departamento' => 14,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Antonio Ilotenango',
            'departamento' => 14,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Bartolomé Jocotenango',
            'departamento' => 14,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Juan Cotzal',
            'departamento' => 14,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Pedro Jocopilas',
            'departamento' => 14,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santa Cruz del Quiché',
            'departamento' => 14,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Uspantán',
            'departamento' => 14,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Zacualpa',
            'departamento' => 14,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Champerico',
            'departamento' => 15,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
        
        DB::table('Municipios')->insert([
            'nombre' => 'El Asintal',
            'departamento' => 15,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Nuevo San Carlos',
            'departamento' => 15,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Retalhuleu',
            'departamento' => 15,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Andrés Villa Seca',
            'departamento' => 15,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Felipe',
            'departamento' => 15,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Martín Zapotitlán',
            'departamento' => 15,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Sebastián',
            'departamento' => 15,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santa Cruz Muluá',
            'departamento' => 15,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Alotenango',
            'departamento' => 16,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Antigua',
            'departamento' => 16,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Ciudad Vieja',
            'departamento' => 16,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Jocotenango',
            'departamento' => 16,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Magdalena Milpas Altas',
            'departamento' => 16,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Pastores',
            'departamento' => 16,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Antonio Aguas Calientes',
            'departamento' => 16,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Bartolomé Milpas Altas',
            'departamento' => 16,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Lucas Sacatepéquez',
            'departamento' => 16,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Miguel Dueñas',
            'departamento' => 16,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santa Catarina Barahona',
            'departamento' => 16,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santa Lucía Milpas Altas',
            'departamento' => 16,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santa María de Jesús',
            'departamento' => 16,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santiago Sacatepéquez',
            'departamento' => 16,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santo Domingo Xenacoj',
            'departamento' => 16,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Sumpango',
            'departamento' => 16,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Ayutla',
            'departamento' => 17,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Catarina',
            'departamento' => 17,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Comitancillo',
            'departamento' => 17,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Concepción Tutuapa',
            'departamento' => 17,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'El Quetzal',
            'departamento' => 17,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'El Rodeo',
            'departamento' => 17,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'El Tumbador',
            'departamento' => 17,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Esquipulas Palo Gordo',
            'departamento' => 17,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Ixchiguan',
            'departamento' => 17,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'La Reforma',
            'departamento' => 17,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Malacatán',
            'departamento' => 17,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Nuevo Progreso',
            'departamento' => 17,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Ocos',
            'departamento' => 17,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Pajapita',
            'departamento' => 17,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Río Blanco',
            'departamento' => 17,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Antonio Sacatepéquez',
            'departamento' => 17,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Cristóbal Cucho',
            'departamento' => 17,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San José Ojetenam',
            'departamento' => 17,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Lorenzo',
            'departamento' => 17,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Marcos',
            'departamento' => 17,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Miguel Ixtahuacán',
            'departamento' => 17,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Pablo',
            'departamento' => 17,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Pedro Sacatepéquez',
            'departamento' => 17,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Rafael Pie de La Cuesta',
            'departamento' => 17,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Sibinal',
            'departamento' => 17,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Sipacapa',
            'departamento' => 17,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Tacaná',
            'departamento' => 17,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Tajumulco',
            'departamento' => 17,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Tejutla',
            'departamento' => 17,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Barberena',
            'departamento' => 18,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Casillas',
            'departamento' => 18,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Chiquimulilla',
            'departamento' => 18,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Cuilapa',
            'departamento' => 18,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Guazacapán',
            'departamento' => 18,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Nueva Santa Rosa',
            'departamento' => 18,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Oratorio',
            'departamento' => 18,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Pueblo Nuevo Viñas',
            'departamento' => 18,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Juan Tecuaco',
            'departamento' => 18,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Rafael Las Flores',
            'departamento' => 18,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santa Cruz Naranjo',
            'departamento' => 18,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santa María Ixhuatán',
            'departamento' => 18,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santa Rosa de Lima',
            'departamento' => 18,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Taxisco',
            'departamento' => 18,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Concepción',
            'departamento' => 19,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Nahualá',
            'departamento' => 19,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Panajachel',
            'departamento' => 19,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Andrés Semetabaj',
            'departamento' => 19,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Antonio Palopó',
            'departamento' => 19,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San José Chacaya',
            'departamento' => 19,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Juan La Laguna',
            'departamento' => 19,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Lucas Tolimán',
            'departamento' => 19,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Marcos La Laguna',
            'departamento' => 19,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Pablo La Laguna',
            'departamento' => 19,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Pedro La Laguna',
            'departamento' => 19,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santa Catarina Ixtahuacan',
            'departamento' => 19,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santa Catarina Palopó',
            'departamento' => 19,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santa Clara La Laguna',
            'departamento' => 19,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santa Cruz La Laguna',
            'departamento' => 19,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santa Lucía Utatlán',
            'departamento' => 19,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santa María Visitación',
            'departamento' => 19,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santiago Atitlán',
            'departamento' => 19,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Sololá',
            'departamento' => 19,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Chicacao',
            'departamento' => 20,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Cuyotenango',
            'departamento' => 20,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Mazatenango',
            'departamento' => 20,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Patulul',
            'departamento' => 20,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Pueblo Nuevo',
            'departamento' => 20,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Río Bravo',
            'departamento' => 20,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Samayac',
            'departamento' => 20,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Antonio Suchitepéquez',
            'departamento' => 20,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Bernardino',
            'departamento' => 20,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Francisco Zapotitlán',
            'departamento' => 20,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Gabriel',
            'departamento' => 20,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San José El Idolo',
            'departamento' => 20,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Juan Bautista',
            'departamento' => 20,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Lorenzo',
            'departamento' => 20,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Miguel Panán',
            'departamento' => 20,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Pablo Jocopilas',
            'departamento' => 20,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santa Bárbara',
            'departamento' => 20,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santo Domingo Suchitepequez',
            'departamento' => 20,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santo Tomas La Unión',
            'departamento' => 20,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Zunilito',
            'departamento' => 20,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Momostenango',
            'departamento' => 21,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Andrés Xecul',
            'departamento' => 21,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Bartolo',
            'departamento' => 21,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Cristóbal Totonicapán',
            'departamento' => 21,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Francisco El Alto',
            'departamento' => 21,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santa Lucía La Reforma',
            'departamento' => 21,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Santa María Chiquimula',
            'departamento' => 21,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Totonicapán',
            'departamento' => 21,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Cabañas',
            'departamento' => 22,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Estanzuela',
            'departamento' => 22,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Gualán',
            'departamento' => 22,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Huité',
            'departamento' => 22,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'La Unión',
            'departamento' => 22,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Río Hondo',
            'departamento' => 22,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'San Diego',
            'departamento' => 22,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Teculután',
            'departamento' => 22,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Usumatlán',
            'departamento' => 22,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Municipios')->insert([
            'nombre' => 'Zacapa',
            'departamento' => 22,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
    }
}

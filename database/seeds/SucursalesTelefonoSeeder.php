<?php

use Illuminate\Database\Seeder;

class SucursalesTelefonoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('SucursalesTelefono')->insert([
            'numero' => '76374673',
            'preferida' => true,
            'sucursal' => 1,
            'tipotelefono' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('SucursalesTelefono')->insert([
            'numero' => '87367473',
            'preferida' => false,
            'sucursal' => 1,
            'tipotelefono' => 2,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('SucursalesTelefono')->insert([
            'numero' => '62434423',
            'preferida' => false,
            'sucursal' => 2,
            'tipotelefono' => 2,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
    }
}
<?php

use Illuminate\Database\Seeder;

class FormaPagoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('FormaPago')->insert([
            'nombre' => 'Efectivo',
            'sedeposita' => true,
            'estado' => true,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('FormaPago')->insert([
            'nombre' => 'Abono',
            'sedeposita' => true,
            'estado' => true,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('FormaPago')->insert([
            'nombre' => 'Cheque',
            'sedeposita' => true,
            'estado' => true,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('FormaPago')->insert([
            'nombre' => 'Tarjeta',
            'sedeposita' => false,
            'estado' => true,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('FormaPago')->insert([
            'nombre' => 'Transferencia',
            'sedeposita' => false,
            'estado' => true,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('FormaPago')->insert([
            'nombre' => 'Nota de Credito',
            'sedeposita' => false,
            'estado' => true,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('FormaPago')->insert([
            'nombre' => 'Credito',
            'sedeposita' => false,
            'estado' => true,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('FormaPago')->insert([
            'nombre' => 'Cheque Prefechado',
            'sedeposita' => false,
            'estado' => true,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('FormaPago')->insert([
            'nombre' => 'Cupon Promocional',
            'sedeposita' => false,
            'estado' => true,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
    }
}

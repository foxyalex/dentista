<?php

use Illuminate\Database\Seeder;

class TipoContactoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('TipoContacto')->insert([
            'nombre' => 'Esposa',
            'descripcion' => 'Encargada de las citas de salud',
            'estado' => true,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('TipoContacto')->insert([
            'nombre' => 'Hijos',
            'descripcion' => 'Encargados del Seguro Medico',
            'estado' => true,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class MedioPublicitarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('MedioPublicitario')->insert([
            'nombre' => 'Ningun Medio',
            'estado' => true,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('MedioPublicitario')->insert([
            'nombre' => 'Televisión',
            'estado' => true,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('MedioPublicitario')->insert([
            'nombre' => 'Prensa',
            'estado' => true,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
    }
}

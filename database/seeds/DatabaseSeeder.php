<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TipoTelefonoSeeder::class);
        $this->call(TipoCorreoSeeder::class);
        $this->call(TipoDireccionSeeder::class);
        $this->call(TipoContactoSeeder::class);
        $this->call(TipoProfresionSeeder::class);
        $this->call(TipoEnfermedadSeeder::class);
        $this->call(PaisesSeeder::class);
        $this->call(DepartamentosSeeder::class);
        $this->call(MunicipiosSeeder::class);
        $this->call(TipoPacienteSeeder::class);
        $this->call(TipoSangreSeeder::class);
        $this->call(FormaPagoSeeder::class);
        $this->call(MotivoConsultaSeeder::class);
        $this->call(PersonaSeeder::class);
        $this->call(PersonaTelefonoSeeder::class);
        $this->call(PersonaCorreoSeeder::class);
        $this->call(PersonaDireccionSeeder::class);
        $this->call(PersonaFacturacionSeeder::class);
        $this->call(MedioPublicitarioSeeder::class);
        $this->call(CentroEmergenciaSeeder::class);
        $this->call(CentroEmergenciaTelefonoSeeder::class);
        $this->call(CentroEmergenciaCorreoSeeder::class);
        $this->call(PacienteSeeder::class);
        $this->call(PacienteEnfermedadSeeder::class);
        $this->call(UsuarioSeeder::class);
        $this->call(SucursalesSeeder::class);
        $this->call(SucursalesCorreoSeeder::class);
        $this->call(SucursalesTelefonoSeeder::class);
    }
}

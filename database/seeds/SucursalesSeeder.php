<?php

use Illuminate\Database\Seeder;

class SucursalesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Sucursales')->insert([
            'corto' => 'CN',
            'nombre' => 'Central',
            'direccion' => '15 Av Zona 15 Guatemala',
            'estado' => true,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('Sucursales')->insert([
            'corto' => 'Z1',
            'nombre' => 'Zona 1',
            'direccion' => '1 Calle 12 Av Zona 1 Guatemala',
            'estado' => true,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
    }
}

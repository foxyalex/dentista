<?php

use Illuminate\Database\Seeder;

class PacienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Paciente')->insert([
            'aspectosrelevantes' => 'Alergico a la Penicilina',
            'polizaseguro' => '123412341234',
            'montoautorizado' => 50000.00,
            'persona' => 1,
            'motivoconsulta' => 1,
            'centroemergencia' => 1,
            'tipopaciente' => 1,
            'tiposangre' => 1,
            'mediopublicitario' => 1,
            'hmproblemas' => 'Ningun Problema',
            'hmcirugias' => 'Si, De rodilla',
            'hmtrastornocorazon' => 1,
            'hmtrastornocorazoncomentario' => 'Ritmo Cardiaco',
            'hmmedicamentos' => 'Ningun Medicamento',
            'hmfuma' => 0,
            'hmbebidas' => 1,
            'hmtratamiento' => 'Ningun Tratamiento',
            'hmalergico' => 'Ninguna Alergia',
            'hmaccidentes' => 'Si, en motocicleta',
            'hmcomentarios' => '',
            'houltimavisita' => 'Hace 15 días',
            'hotratamientodental' => 'Si, Ortodoncia',
            'hocomentariosrelevantes' => 'Ninguno',
            'homolestiaactual' => 0,
            'hoaccidentes' => 1,
            'hoextraccion' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
    }
}

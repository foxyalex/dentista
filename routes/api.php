<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('paises/{id}/departamentos', 'PaisesController@departamentos');
Route::get('departamentos/{id}/municipios', 'DepartamentosController@municipios');
Route::get('persona/{id}/telefonos', 'PersonaController@telefonos');
Route::get('persona/{id}/correos', 'PersonaController@correos');
Route::get('persona/{id}/direcciones', 'PersonaController@direcciones');
Route::get('persona/{id}/facturacion', 'PersonaController@facturacion');
Route::get('centroemergencia/{id}/telefonos', 'CentroEmergenciaController@telefonos');
Route::get('centroemergencia/{id}/correos', 'CentroEmergenciaController@correos');
Route::get('paciente/{id}/enfermedades', 'PacienteController@enfermedades');
Route::get('sucursales/{id}/telefonos', 'SucursalesController@telefonos');
Route::get('sucursales/{id}/correos', 'SucursalesController@correos');

Route::resource('tipotelefono', 'TipoTelefonoController');
Route::resource('tipocorreo', 'TipoCorreoController');
Route::resource('tipodireccion', 'TipoDireccionController');
Route::resource('tipocontacto', 'TipoContactoController');
Route::resource('tipoprofesion', 'TipoProfesionController');
Route::resource('tipoenfermedad', 'TipoEnfermedadController');
Route::resource('paises', 'PaisesController');
Route::resource('departamentos', 'DepartamentosController');
Route::resource('municipios', 'MunicipiosController');
Route::resource('tipopaciente', 'TipoPacienteController');
Route::resource('tiposangre', 'TipoSangreController');
Route::resource('formapago', 'FormaPagoController');
Route::resource('motivoconsulta', 'MotivoConsultaController');
Route::resource('mediopublicitario', 'MedioPublicitarioController');
Route::resource('persona', 'PersonaController');
Route::resource('personatelefono', 'PersonaTelefonoController');
Route::resource('personacorreo', 'PersonaCorreoController');
Route::resource('personadireccion', 'PersonaDireccionController');
Route::resource('personafacturacion', 'PersonaFacturacionController');
Route::resource('paciente', 'PacienteController');
Route::resource('pacienteenfermedad', 'PacienteEnfermedadController');
Route::resource('centroemergencia', 'CentroEmergenciaController');
Route::resource('centroemergenciatelefono', 'CentroEmergenciaTelefonoController');
Route::resource('centroemergenciacorreo', 'CentroEmergenciaCorreoController');
Route::resource('usuario', 'UsuarioController');
Route::resource('sucursales', 'SucursalesController');
Route::resource('sucursalescorreo', 'SucursalesCorreoController');
Route::resource('sucursalestelefono', 'SucursalesTelefonoController');
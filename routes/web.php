<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::get('/login', function () {
    return view('login');
});

Route::get('/recover', function () {
    return view('recover');
});

Route::get('/panel', function () {
    return view('layouts.panel');
});

Route::group(['prefix' => 'layouts'], function()
{
    Route::get('dashboard', function(){ return view('layouts.dashboard'); });
    Route::get('tipotelefono', function(){ return view('layouts.tipotelefono'); });
    Route::get('tipocorreo', function(){ return view('layouts.tipocorreo'); });
    Route::get('tipodireccion', function(){ return view('layouts.tipodireccion'); });
    Route::get('tipocontacto', function(){ return view('layouts.tipocontacto'); });
    Route::get('tipoprofesion', function(){ return view('layouts.tipoprofesion'); });
    Route::get('tipoenfermedad', function(){ return view('layouts.tipoenfermedad'); });
    Route::get('paises', function(){ return view('layouts.paises'); });
    Route::get('tipopaciente', function(){ return view('layouts.tipopaciente'); });
    Route::get('tiposangre', function(){ return view('layouts.tiposangre'); });
    Route::get('formapago', function(){ return view('layouts.formapago'); });
    Route::get('motivoconsulta', function(){ return view('layouts.motivoconsulta'); });
    Route::get('mediopublicitario', function(){ return view('layouts.mediopublicitario'); });
    Route::get('pacientes', function(){ return view('layouts.pacientes'); });
    Route::get('centroemergencia', function(){ return view('layouts.centroemergencia'); });
    Route::get('usuarios', function(){ return view('layouts.usuarios'); });
    Route::get('sucursales', function(){ return view('layouts.sucursales'); });
});